package com.bovquier.resume.base.view.viewmodels

import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import com.bovquier.resume.R
import com.bovquier.resume.common.CommonExceptions
import com.bovquier.resume.common.LiveDataAction
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.errorResponse
import com.bovquier.resume.setup.getErrorJsonResponse
import io.mockk.CapturingSlot
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import io.mockk.verifySequence
import org.spekframework.spek2.style.gherkin.Feature
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class BaseViewModelSpek : BaseResumeSpek({
    Feature("BaseViewModelSpek") {
        val viewModel = DummyViewModel()
        val toastStringObserver = mockk<Observer<LiveDataAction<String>>>()
        val toastResObserver = mockk<Observer<LiveDataAction<Int>>>()

        viewModel.toastResData.observeForever(toastResObserver)
        viewModel.toastStrData.observeForever(toastStringObserver)

        val intSlot = CapturingSlot<LiveDataAction<Int>>()
        val stringSlot = CapturingSlot<LiveDataAction<String>>()

        beforeEachTest {
            clearMocks(toastStringObserver, toastResObserver)
            intSlot.clear()
            stringSlot.clear()
        }

        Scenario("Showing toast messages") {
            Then("Should notify string message toast") {
                viewModel.showStringMessage("message-123")
                verify { toastStringObserver.onChanged(capture(stringSlot)) }
                assertEquals("message-123", stringSlot.captured.value)
            }
            Then("Should notify resource message toast") {
                viewModel.showResourceMessage(R.string.app_name)
                verify { toastResObserver.onChanged(capture(intSlot)) }
                assertEquals(R.string.app_name, intSlot.captured.value)
            }
        }
        Scenario("Handling errors") {
            Then("Should handle DB error") {
                viewModel.manageError(CommonExceptions.MissingDatabaseItem())
                verify { toastResObserver.onChanged(capture(intSlot)) }
                assertEquals(R.string.missing_item, intSlot.captured.value)
            }
            Then("Should handle specified Api Error") {
                val apiCallException = spyk(CommonExceptions.ApiCallException.Builder(getErrorJsonResponse(errorResponse)).build())
                viewModel.manageError(apiCallException)
                verify { toastStringObserver.onChanged(capture(stringSlot)) }
                val captured = stringSlot.captured.value
                assertEquals(apiCallException.errorPojo?.message, captured)
                assertNotNull(captured)
            }
            Then("Should handle unknown Api Error") {
                val apiCallException = spyk(CommonExceptions.ApiCallException.Builder(getErrorJsonResponse(errorResponse)).build())
                every { apiCallException.errorPojo?.errorId } returns 0
                viewModel.manageError(apiCallException)
                verify { toastStringObserver.onChanged(capture(stringSlot)) }
                val captured = stringSlot.captured.value
                assertNotNull(captured)
                verifySequence {
                    apiCallException.errorPojo?.also {
                        it.errorId
                        it.error
                        it.errorId
                    }
                }
            }
            Then("Should reach unspecified error code") {
                val error = mockk<Exception>()
                viewModel.manageError(error)
                verify { error.localizedMessage }
            }
        }
    }
})

private class DummyViewModel : BaseViewModel() {
    fun showResourceMessage(@StringRes msgId: Int) {
        showMessage(msgId)
    }

    fun showStringMessage(msg: String) {
        showMessage(msg)
    }

    fun manageError(error: Throwable) {
        handleError(error)
    }
}