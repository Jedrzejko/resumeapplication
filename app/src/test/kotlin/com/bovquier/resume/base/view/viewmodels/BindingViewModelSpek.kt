package com.bovquier.resume.base.view.viewmodels

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.Observer
import com.bovquier.resume.setup.BaseResumeSpek
import io.mockk.clearMocks
import io.mockk.mockk
import io.mockk.verify
import org.spekframework.spek2.style.gherkin.Feature

class BindingViewModelSpek : BaseResumeSpek({
    Feature("BindingViewModelSpek") {
        val callbacks = mockk<PropertyChangeRegistry>()
        val viewModel = BindingViewModel(callbacks)

        beforeEachTest { clearMocks(callbacks) }

        Scenario("Managing callbacks") {
            val dummyCallback = mockk<Observable.OnPropertyChangedCallback>(relaxed = false)
            Then("Should add new callback") {
                viewModel.addOnPropertyChangedCallback(dummyCallback)
                verify(exactly = 1) { callbacks.add(dummyCallback) }
            }
            Then("Should remove new callback") {
                viewModel.removeOnPropertyChangedCallback(dummyCallback)
                verify(exactly = 1) { callbacks.remove(dummyCallback) }
            }
        }
        Scenario("Notifying callbacks") {
            Then("Should notify all callbacks") {
                viewModel.notifyChange()
                verify(exactly = 1) { callbacks.notifyCallbacks(viewModel, 0, null) }
            }
            Then("Should notify field specified callbacks") {
                viewModel.notifyPropertyChanged(123)
                verify(exactly = 1) { callbacks.notifyCallbacks(viewModel, 123, null) }
            }
        }
        Scenario("Force binding") {
            val forcedObserver = mockk<Observer<Int>>()
            beforeEachTest { clearMocks(forcedObserver) }
            viewModel.forceBinding.observeForever(forcedObserver)
            Then("Should force unspecified field") {
                viewModel.forceBinding()
                verify(exactly = 1) { forcedObserver.onChanged(-1) }
            }
            Then("Should force specific field") {
                viewModel.forceBinding(123)
                verify(exactly = 1) { forcedObserver.onChanged(123) }
            }
        }
    }
})