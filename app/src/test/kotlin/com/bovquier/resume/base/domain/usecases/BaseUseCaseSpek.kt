package com.bovquier.resume.base.domain.usecases

import com.bovquier.resume.api.connectors.EducationConnector
import com.bovquier.resume.base.api.repositories.BaseRepository
import com.bovquier.resume.common.CommonExceptions.MissingDatabaseItem
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.data.pojo.CommonListResponse
import com.bovquier.resume.data.pojo.EducationPojo
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.TestCoroutineDispatcher
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.spekframework.spek2.style.gherkin.Feature
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

@ExperimentalCoroutinesApi
object BaseUseCaseSpek : BaseResumeSpek({
    Feature("BaseUseCaseSpek") {
        Scenario("Simple execution") {
            val apiCaller = mockk<BaseApiCaller>(relaxed = true)
            val useCase = BaseUseCaseCaller(apiCaller, TestCoroutineDispatcher())

            beforeEachStep {
                clearMocks(apiCaller)
                useCase.blockDelay = 0L
            }

            Then("Should perform use case operation") {
                coEvery { apiCaller.getSomeOtherData(1) } returns getEduResponse("234")
                var result = "---"
                runBlocking { useCase.execute(1) { result = it.result?.rows?.get(0)?.finishYear ?: "---" } }
                assertEquals("234", result)
            }

            Then("Should return MissingDatabaseItemException") {
                coEvery { apiCaller.getSomeOtherData(1) } throws MissingDatabaseItem()
                var result: UseCaseResult<CommonListResponse<EducationPojo.EducationItem>> = UseCaseResult.withResult(getEduResponse("234"))
                runBlocking { useCase.execute(1) { result = it } }
                assertNull(result.result)
                assertTrue { result.error is MissingDatabaseItem }
            }

            Then("Should return common exception") {
                coEvery { apiCaller.getSomeOtherData(1) } throws IllegalArgumentException()
                var result: UseCaseResult<CommonListResponse<EducationPojo.EducationItem>> = UseCaseResult.withResult(getEduResponse("234"))
                runBlocking { useCase.execute(1) { result = it } }
                assertNull(result.result)
                assertTrue { result.error is IllegalArgumentException }
            }

            Then("Should return no exception nor result on cancel") {
                useCase.blockDelay = 100L
                coEvery { apiCaller.getSomeOtherData(1) } returns getEduResponse("234")
                var result: UseCaseResult<CommonListResponse<EducationPojo.EducationItem>> = UseCaseResult.withResult(getEduResponse("234"))
                runBlocking {
                    useCase.execute(1) { result = it } //this one should be cancelled and no new result will be set.
                    useCase.execute(1) { }
                }
                assertEquals("234", result.result?.rows?.get(0)?.finishYear)
                assertNull(result.error)
            }
        }
    }
})

class BaseUseCaseCaller(private val apiCaller: BaseApiCaller, private val dispatchers: ResumeCoroutineDispatcher) :
    BaseUseCase<Int, CommonListResponse<EducationPojo.EducationItem>>(dispatchers) {
    var blockDelay: Long = 0L

    override suspend fun executeOnBackground(params: Int): CommonListResponse<EducationPojo.EducationItem> = withContext(CoroutineScope(dispatchers.bgContext).coroutineContext) {
        //normal calls don;t need delay, but for CANCEL spek, execution should be delayed,
        //so the "execute" will actually cancel previously started job.
        delay(blockDelay)
        apiCaller.getSomeOtherData(1)
    }
}

class BaseApiCaller(private val educationConnector: EducationConnector) : BaseRepository() {
    suspend fun getSomeOtherData(path: Int) = performSimpleApiCall { educationConnector.fetchEducation(path.toString()) }
}

@Suppress("SameParameterValue")
private fun getEduResponse(year: String) = CommonListResponse(
    rows = listOf(
        EducationPojo.EducationItem(year, "qwer", "asdf", "zxcv", EducationPojo.ProjectItem("rfv", "tgb"))
    )
)