package com.bovquier.resume.setup

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.spekframework.spek2.Spek
import org.spekframework.spek2.dsl.Root

abstract class BaseResumeSpek(
    val spek: Root.() -> Unit
) : Spek({
    /** after {@link androidx.arch.core.executor.testing.InstantTaskExecutorRule}
     * In order to test LiveData, the `InstantTaskExecutorRule` rule needs to be applied via JUnit.
     * As we are running it with Spek, the "rule" will be implemented in this way instead
     */
    setupInstantRunExecutor()
    beforeEachTest { setupInstantRunExecutor() }
    beforeGroup { setupInstantRunExecutor() }
    afterEachTest { ArchTaskExecutor.getInstance().setDelegate(null) }
    afterGroup { ArchTaskExecutor.getInstance().setDelegate(null) }
    defaultTimeout = 60000L //increased coroutine timeout for debugging to 1min
    spek()
})

fun setupInstantRunExecutor() {
    ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
        override fun executeOnDiskIO(runnable: Runnable) {
            runnable.run()
        }

        override fun isMainThread(): Boolean {
            return true
        }

        override fun postToMainThread(runnable: Runnable) {
            runnable.run()
        }
    })
}

fun getErrorJsonResponse(error: String): ResponseBody = error.toResponseBody("application/json".toMediaTypeOrNull())
val errorResponse
    get() =
        """
            {
               "error_id":123,
               "error":"WRONG_PATH",
               "message":"Provided data has been destroyed"
            }
        """.trimIndent()