package com.bovquier.resume.setup

import com.bovquier.resume.data.db.Education
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.data.db.Models
import io.requery.Converter
import io.requery.Persistable
import io.requery.android.DefaultMapping
import io.requery.cache.EmptyEntityCache
import io.requery.sql.Configuration
import io.requery.sql.ConfigurationBuilder
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.Platform
import io.requery.sql.SchemaModifier
import io.requery.sql.TableCreationMode
import io.requery.sql.platform.SQLite
import org.junit.rules.TemporaryFolder
import org.sqlite.SQLiteConfig
import org.sqlite.SQLiteDataSource
import java.util.concurrent.Executors
import kotlin.reflect.KClass

class TestDataStore private constructor(private val configuration: Configuration, val data: KotlinEntityDataStore<Persistable>) {
    fun recreateTables(): Long {
        val schemaModifier = SchemaModifier(configuration)
        schemaModifier.createTables(TableCreationMode.DROP_CREATE)
        return -1L
    }

    class Builder(folder: TemporaryFolder) {
        private val platform: Platform = SQLite()
        private val configurationBuilder: ConfigurationBuilder

        fun converters(converters: Set<Converter<*, *>>): Builder {
            val defaultMapping = DefaultMapping(platform)
            for (converter in converters) {
                defaultMapping.addConverter(converter)
            }
            configurationBuilder.setMapping(defaultMapping)
            return this
        }

        fun build(): TestDataStore {
            val configuration = configurationBuilder.build()
            val tables = SchemaModifier(configuration)
            tables.createTables(TableCreationMode.DROP_CREATE)
            return TestDataStore(configuration, KotlinEntityDataStore(configuration))
        }

        init {
            folder.create()
            val dataSource = SQLiteDataSource()
            dataSource.url = "jdbc:sqlite:" + folder.newFile().absolutePath
            val config = SQLiteConfig()
            config.setDateClass("TEXT")
            dataSource.config = config
            dataSource.setEnforceForeignKeys(true)
            val model = Models.DEFAULT
            configurationBuilder = ConfigurationBuilder(dataSource, model)
                .setEntityCache(EmptyEntityCache())
                .setWriteExecutor(Executors.newSingleThreadExecutor())
        }
    }
}

class Helper(private val database: KotlinEntityDataStore<Persistable>) {

    fun <E : Persistable> insertValue(entity: E): E = database.insert(entity)

    private fun <E : Persistable> insertValueList(entities: MutableList<E>): MutableList<E> =
        database.insert(entities).toMutableList()

    fun <E : Persistable> selectEntity(kClass: KClass<E>): E = database.select(kClass).get().first()

    fun <E : Persistable> selectEntityList(kClass: KClass<E>): Iterable<E> =
        database.select(kClass).get().toList()

    fun insertEducation(): List<IEducation> = insertValueList(mutableListOf(
        Education().apply { finishYear = "1234" },
        Education().apply { finishYear = "2345" }
    ))
}