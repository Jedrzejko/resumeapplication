package com.bovquier.resume.setup

import com.bovquier.resume.common.ResumeCoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlin.coroutines.CoroutineContext

@ExperimentalCoroutinesApi
class TestCoroutineDispatcher : ResumeCoroutineDispatcher {
    override val bgContext: CoroutineContext
        get() = Dispatchers.Unconfined

    override val fgContext: CoroutineContext
        get() = Dispatchers.Unconfined
}