package com.bovquier.resume.api.repositories

import com.bovquier.resume.api.connectors.EducationConnector
import com.bovquier.resume.api.db.DatabaseController
import com.bovquier.resume.common.CommonExceptions
import com.bovquier.resume.data.db.EducationEntities
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.data.pojo.CommonListResponse
import com.bovquier.resume.data.pojo.EducationPojo
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.getErrorJsonResponse
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifySequence
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.spekframework.spek2.style.gherkin.Feature
import retrofit2.Response
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EducationRepositorySpek : BaseResumeSpek({
    Feature("EducationRepositorySpek") {
        val dbController = mockk<DatabaseController>()
        val connector = mockk<EducationConnector>()
        val repo = EducationRepository(connector, dbController)

        beforeEachTest { clearMocks(connector, dbController) }
        Scenario("Fetching education data") {
            Then("Should perform full fetch sequence") {
                coEvery { dbController.clearItems(IEducation::class) } returns 1
                coEvery { connector.fetchEducation(fileName) } returns Response.success(fetchResponse)
                coEvery { dbController.saveEntities(any<List<IEducation>>()) } returns entities

                runBlocking { repo.fetchEducation(fileName) }

                coVerifySequence {
                    dbController.clearItems(IEducation::class)
                    connector.fetchEducation(fileName)
                    dbController.saveEntities(entities)
                }
            }
            Then("Should skip clearing on fetching") {
                coEvery { dbController.clearItems(IEducation::class) } returns 1
                coEvery { connector.fetchEducation(fileName) } returns Response.success(fetchResponse)
                coEvery { dbController.saveEntities(any<List<IEducation>>()) } returns entities

                runBlocking { repo.fetchEducation(fileName, false) }

                coVerifySequence {
                    connector.fetchEducation(fileName)
                    dbController.saveEntities(entities)
                }
                coVerify(exactly = 0) { dbController.clearItems(IEducation::class) }
            }
            Then("Should fail fetching items") {
                coEvery { dbController.clearItems(IEducation::class) } returns 1
                coEvery { connector.fetchEducation(fileName) } returns Response.error(400, getErrorJsonResponse("{}"))
                assertFailsWith(CommonExceptions.ApiCallException::class) {
                    runBlocking { repo.fetchEducation(fileName) }
                }
            }
        }
        Scenario("Loading education data") {
            coEvery { dbController.retrieveEducation(123) } returns entities[0]
            val result = runBlocking { repo.loadEducation(123) }
            assertEquals(entities[0].finishYear, result.finishYear)
        }
    }
})

private const val fileName = "asd"
private val resultItem = EducationPojo.EducationItem("1234", "qwer", "asdf", "zxcv", EducationPojo.ProjectItem("rfv", "tgb"))
private val fetchResponse = CommonListResponse(rows = listOf(resultItem))
val entities = EducationEntities.mapFromResponse(fetchResponse)