package com.bovquier.resume.api.db

import com.bovquier.resume.api.di.requeryConverters
import com.bovquier.resume.common.CommonExceptions
import com.bovquier.resume.data.db.Education
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.data.db.IProject
import com.bovquier.resume.data.db.Project
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.Helper
import com.bovquier.resume.setup.TestDataStore
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.rules.TemporaryFolder
import org.spekframework.spek2.style.gherkin.Feature
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class DatabaseControllerSpek : BaseResumeSpek({
    Feature("DatabaseControllerSpek") {
        val folder = TemporaryFolder()
        val store: TestDataStore = TestDataStore.Builder(folder)
            .converters(requeryConverters)
            .build()
        lateinit var database: KotlinEntityDataStore<Persistable>
        lateinit var dbController: DatabaseController

        lateinit var helper: Helper
        beforeEachTest {
            database = store.data
            store.recreateTables()
            dbController = DatabaseController(database, Dispatchers.Unconfined)
            helper = Helper(database)
        }
        afterEachTest { database.delete() }
        Scenario("Basic database data controls") {
            Then("Should fail on no item") {
                assertFailsWith(CommonExceptions.MissingDatabaseItem::class) {
                    runBlocking { dbController.retrieveEducation(1234562345) }
                }
            }
            Then("should save any entity") {
                runBlocking {
                    dbController.saveEntity(Education().apply { finishYear = "1234" })
                    val selectEntity = helper.selectEntity(IEducation::class)
                    assertEquals("1234", selectEntity.finishYear)
                }
                runBlocking {
                    dbController.saveEntity(Project().apply { projectName = "pName" })
                    val selectEntity = helper.selectEntity(IProject::class)
                    assertEquals("pName", selectEntity.projectName)
                }
            }

            Then("should save any entity list") {
                runBlocking {
                    dbController.saveEntities(listOf(Education().apply { finishYear = "1234" }))
                    val selectEntity = helper.selectEntityList(IEducation::class)
                    assertEquals("1234", selectEntity.toMutableList()[0].finishYear)
                }
                runBlocking {
                    dbController.saveEntity(Project().apply { projectName = "pName" })
                    val selectEntity = helper.selectEntityList(IProject::class)
                    assertEquals("pName", selectEntity.toMutableList()[0].projectName)
                }
            }
            Then("should clear edu items") {
                val insertions = helper.insertEducation()
                val cleared = runBlocking { dbController.clearItems(IEducation::class) }
                val size = helper.selectEntityList(IEducation::class).toList().size
                assertEquals(insertions.size, cleared)
                assertEquals(0, size)
            }
        }
        Scenario("Education controlls") {
            Then("Should retrieve education item") {
                val insertions = helper.insertEducation()
                runBlocking {
                    val edu = dbController.retrieveEducation(insertions[0].educationId)
                    assertEquals(insertions[0].finishYear, edu.finishYear)
                }
            }
        }
    }
})