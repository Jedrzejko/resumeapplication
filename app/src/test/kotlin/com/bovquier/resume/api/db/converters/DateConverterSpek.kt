package com.bovquier.resume.api.db.converters

import com.bovquier.resume.setup.BaseResumeSpek
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.spekframework.spek2.style.gherkin.Feature
import java.util.*

class DateConverterSpek : BaseResumeSpek({
    Feature("DateConverterSpek") {
        val converter = DateConverter()

        Scenario("Basic configs") {
            Then("Should pass proper persisted type") {
                assertEquals(Long::class.java, converter.persistedType)
            }

            Then("Should pass proper mapped type") {
                assertEquals(Date::class.java, converter.mappedType)
            }

            Then("Should pass NULL persisted size") {
                assertNull(converter.persistedSize)
            }
        }
        Scenario("Conversion to persisted type: Date") {
            data class TestCase(val input: Long? = null, val expectedOutput: Date?)
            listOf(
                TestCase(null, null),
                TestCase(0L, null),
                TestCase(1L, Date(1L))
            ).forEachIndexed { index, testCase ->
                Then("#$index: Should convert to proper persisted Date") {
                    assertEquals(testCase.expectedOutput?.time, converter.convertToMapped(testCase.input)?.time)
                }
            }
        }
        Scenario("Conversion to mapped type: Long") {
            data class TestCase(val input: Date?, val expectedOutput: Long?)
            listOf(
                TestCase(null, null),
                TestCase(Date(0L), 0L),
                TestCase(Date(1L), 1L)
            ).forEachIndexed { index, testCase ->
                Then("#$index: Should convert to proper persisted Date") {
                    assertEquals(testCase.expectedOutput, converter.convertToPersisted(testCase.input))
                }
            }
        }
    }
})