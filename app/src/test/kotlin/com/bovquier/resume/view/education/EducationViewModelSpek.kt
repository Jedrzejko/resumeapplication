package com.bovquier.resume.view.education

import androidx.lifecycle.Observer
import com.bovquier.resume.base.domain.usecases.UseCaseResult
import com.bovquier.resume.common.CommonExceptions.ApiCallException
import com.bovquier.resume.common.LiveDataAction
import com.bovquier.resume.data.db.Education
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.domain.usecases.ActionType
import com.bovquier.resume.domain.usecases.education.EducationUseCase
import com.bovquier.resume.domain.usecases.education.EducationUseCaseParams
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.errorResponse
import com.bovquier.resume.setup.getErrorJsonResponse
import com.bovquier.resume.view.education.EducationViewModel.Companion.EDU_PATH
import com.bovquier.resume.view.education.EducationViewModel.Companion.FAKE_PATH
import io.mockk.CapturingSlot
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.invoke
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.spekframework.spek2.style.gherkin.Feature
import kotlin.test.assertEquals

// TODO : update regarding item clicking; Fix structure, to match Feature-Scenario:Give-When-Then :/
object EducationViewModelSpek : BaseResumeSpek({
    Feature("EducationViewModelSpek") {
        val useCase = mockk<EducationUseCase>()
        val viewModel = EducationViewModel(useCase)

        Scenario("UseCase loadings") {
            val openEduSpyk = spyk(resultItem)
            every { openEduSpyk.educationId } returns 123 //GeneratedKey IDs cannot be set
            val adapterItemsObserver = mockk<Observer<List<IEducation>>>()
            viewModel.adapterItems.observeForever(adapterItemsObserver)
            beforeEachStep { clearMocks(useCase, adapterItemsObserver) }

            Then("Should load proper list items") {
                every { useCase.execute(EducationUseCaseParams(loadPath = EDU_PATH), captureLambda()) } answers {
                    lambda<(UseCaseResult<List<IEducation>>) -> Unit>().invoke(UseCaseResult.withResult(listOf(openEduSpyk)))
                }
                viewModel.loadEducation()
                val slot = CapturingSlot<List<IEducation>>()
                verify(exactly = 1) { adapterItemsObserver.onChanged(capture(slot)) }
                assertEquals(123, slot.captured[0].educationId)
                assertEquals("2010", slot.captured[0].finishYear)
            }

            Then("Should perform fake loading") {
                val apiCallException = spyk(ApiCallException.Builder(getErrorJsonResponse(errorResponse)).build())
                every { useCase.execute(EducationUseCaseParams(ActionType.FOR_ERROR, loadPath = FAKE_PATH), captureLambda()) } answers {
                    lambda<(UseCaseResult<List<IEducation>>) -> Unit>().invoke(UseCaseResult.withError(apiCallException))
                }
                val toastObserver = mockk<Observer<LiveDataAction<String>>>()
                viewModel.toastStrData.observeForever(toastObserver)

                viewModel.loadFakeData()
                verify { apiCallException.errorPojo }

                val slot = CapturingSlot<LiveDataAction<String>>()
                verify { toastObserver.onChanged(capture(slot)) }
                assertEquals(apiCallException.errorPojo?.message, slot.captured.value)
            }
        }
/*
        Scenario("Open details") {
            val detailsNaviObserver = mockk<Observer<LiveDataAction<IEducation>>>()
            viewModel.educationOpenData.observeForever(detailsNaviObserver)
            beforeEachTest { clearMocks(detailsNaviObserver) }

            Then("Should open details item") {
                val openEduSpyk = spyk(resultItem)
                every { openEduSpyk.educationId } returns 123 //GeneratedKey IDs cannot be set
                viewModel.onEducationClicked(openEduSpyk)
                val slot = CapturingSlot<LiveDataAction<IEducation>>()
                verify(exactly = 1) { detailsNaviObserver.onChanged(capture(slot)) }
                assertEquals(123, slot.captured.value?.educationId)
                assertNull(slot.captured.value)
            }
        }
*/
    }
})

private val resultItem = Education().apply {
    school = "some-school"
    finishYear = "2010"
}