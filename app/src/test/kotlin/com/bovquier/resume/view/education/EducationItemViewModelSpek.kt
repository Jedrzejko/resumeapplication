package com.bovquier.resume.view.education

import androidx.databinding.Observable
import com.bovquier.resume.BR
import com.bovquier.resume.data.db.Education
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.setup.BaseResumeSpek
import io.mockk.mockk
import io.mockk.verify
import org.spekframework.spek2.style.gherkin.Feature
import kotlin.test.assertEquals
import kotlin.test.assertNull

// TODO : update regarding item clicking; Fix structure, to match Feature-Scenario:Give-When-Then :/
class EducationItemViewModelSpek : BaseResumeSpek({
    Feature("EducationItemViewModelSpek") {
        val pareViewModel = mockk<EducationViewModel>()
        Scenario("Item data setting") {
            val viewModel = EducationItemViewModel { }
            val propertyCallback: Observable.OnPropertyChangedCallback = mockk()
            viewModel.addOnPropertyChangedCallback(propertyCallback)
            viewModel.item = education
            Then("Should notify data binders") {
                verify(exactly = 1) { propertyCallback.onPropertyChanged(viewModel, BR.itemSubTitle) }
                verify(exactly = 1) { propertyCallback.onPropertyChanged(viewModel, BR.itemTitle) }
            }
            Then("Should have proper item setup") {
                assertEquals("${education.finishYear} - ${education.title}", viewModel.itemSubTitle)
                assertEquals(education.school, viewModel.itemTitle)
            }
            Then("Should have empty binder data") {
                viewModel.item = null
                assertNull(viewModel.itemSubTitle)
                assertNull(viewModel.itemTitle)
            }
        }
/*
        Scenario("Item clicking") {
            val viewModel = EducationItemViewModel(pareViewModel)
            Then("Should not notify listener on empty item click") {
                viewModel.onItemClicked()
                verify(exactly = 0) { pareViewModel.onEducationClicked(any()) }
            }
            Then("Should notify clicker with filled item") {
                viewModel.item = education
                viewModel.onItemClicked()
                val slot = CapturingSlot<IEducation>()
                verify(exactly = 1) { pareViewModel.onEducationClicked(capture(slot)) }
                assertEquals(education.finishYear, slot.captured.finishYear)
            }
        }
*/
    }
})

private val education: IEducation = Education()
    .apply {
        finishYear = "1234"
        title = "asdf"
        school = "qwer"
    }