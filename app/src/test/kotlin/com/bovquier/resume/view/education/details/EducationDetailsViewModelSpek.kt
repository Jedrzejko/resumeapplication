package com.bovquier.resume.view.education.details

import androidx.lifecycle.Observer
import com.bovquier.resume.api.repositories.entities
import com.bovquier.resume.base.domain.usecases.UseCaseResult
import com.bovquier.resume.common.CommonExceptions
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.domain.usecases.education.EducationDetailsUseCase
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.errorResponse
import io.mockk.every
import io.mockk.invoke
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.spekframework.spek2.style.gherkin.Feature

class EducationDetailsViewModelSpek : BaseResumeSpek({
    Feature("EducationDetailsViewModelSpek") {
        val useCase = mockk<EducationDetailsUseCase>()
        Scenario("Should load edu details data") {
            val viewModel = EducationDetailsViewModel(1234, useCase)
            val eduNameObserver = mockk<Observer<String>>()
            val eduFinishResultObserver = mockk<Observer<String>>()
            val eduFosObserver = mockk<Observer<String>>()
            val edusubTitleObserver = mockk<Observer<String>>()
            val edusubDescObserver = mockk<Observer<String>>()
            viewModel.schoolName.observeForever(eduNameObserver)
            viewModel.schoolFinishResult.observeForever(eduFinishResultObserver)
            viewModel.schoolFos.observeForever(eduFosObserver)
            viewModel.subjectTitle.observeForever(edusubTitleObserver)
            viewModel.subjectDescription.observeForever(edusubDescObserver)
            every { useCase.execute(1234, captureLambda()) } answers {
                lambda<(UseCaseResult<IEducation>) -> Unit>().invoke(UseCaseResult.withResult(eduSpyk))
            }
            Then("Should notify UI LiveData with edu properties") {
                viewModel.loadEducationDetails()
                verify(exactly = 1) {
                    eduNameObserver.onChanged(eduSpyk.school)
                    eduFinishResultObserver.onChanged("${eduSpyk.finishYear} - ${eduSpyk.title}")
                    eduFosObserver.onChanged(eduSpyk.fos)
                    edusubTitleObserver.onChanged(eduSpyk.project.projectName)
                    edusubDescObserver.onChanged(eduSpyk.project.projectDescription)
                }
            }
        }
        Scenario("Should load edu details data with exception") {
            val viewModel = EducationDetailsViewModel(2345, useCase)
            val eduNameObserver = mockk<Observer<String>>()
            val eduFinishResultObserver = mockk<Observer<String>>()
            val eduFosObserver = mockk<Observer<String>>()
            val edusubTitleObserver = mockk<Observer<String>>()
            val edusubDescObserver = mockk<Observer<String>>()
            viewModel.schoolName.observeForever(eduNameObserver)
            viewModel.schoolFinishResult.observeForever(eduFinishResultObserver)
            viewModel.schoolFos.observeForever(eduFosObserver)
            viewModel.subjectTitle.observeForever(edusubTitleObserver)
            viewModel.subjectDescription.observeForever(edusubDescObserver)
            every { useCase.execute(2345, captureLambda()) } answers {
                lambda<(UseCaseResult<List<IEducation>>) -> Unit>().invoke(UseCaseResult.withError(apiCallException))
            }
            Then("Should skip UI LiveData notification on Error") {
                viewModel.loadEducationDetails()
                verify(exactly = 0) {
                    eduNameObserver.onChanged(any())
                    eduFinishResultObserver.onChanged(any())
                    eduFosObserver.onChanged(any())
                    edusubTitleObserver.onChanged(any())
                    edusubDescObserver.onChanged(any())
                }
            }
        }
    }
})

val eduSpyk
    get() = entities[0]
val responseBody: ResponseBody = errorResponse.toResponseBody("application/json".toMediaTypeOrNull())
val apiCallException = spyk(CommonExceptions.ApiCallException.Builder(responseBody).build())