package com.bovquier.resume.view.profile

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.Observer
import com.bovquier.resume.base.domain.usecases.UseCaseResult
import com.bovquier.resume.common.LiveDataAction
import com.bovquier.resume.data.pojo.ProfilePojo
import com.bovquier.resume.data.pojo.ProfilePojo.Contact
import com.bovquier.resume.data.pojo.ProfilePojo.Language
import com.bovquier.resume.data.pojo.ProfilePojo.Profile
import com.bovquier.resume.data.pojo.ProfilePojo.SocialLink
import com.bovquier.resume.domain.usecases.profile.ProfileUseCase
import com.bovquier.resume.setup.BaseResumeSpek
import io.mockk.CapturingSlot
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.invoke
import io.mockk.mockk
import io.mockk.verify
import org.spekframework.spek2.style.gherkin.Feature
import kotlin.test.assertEquals
import kotlin.test.assertNull

class ProfileViewModelSpek : BaseResumeSpek({
    Feature("ProfileViewModelSpek") {
        val useCase = mockk<ProfileUseCase>()
        val viewModel = ProfileViewModel(useCase)

        val contactVisibilityObserver = mockk<Observer<Int>>()
        val languagesVisibilityObserver = mockk<Observer<Int>>()
        val socialsVisibilityObserver = mockk<Observer<Int>>()
        viewModel.contactVisibility.observeForever(contactVisibilityObserver)
        viewModel.languagesVisibility.observeForever(languagesVisibilityObserver)
        viewModel.socialsVisibility.observeForever(socialsVisibilityObserver)

        beforeEachTest {
            clearMocks(contactVisibilityObserver, languagesVisibilityObserver, socialsVisibilityObserver, useCase)
        }

        Scenario("Loading full profile data") {
            data class TestCase(val profile: Profile, val visibleCount: Int, val goneCount: Int)
            listOf(
                TestCase(goodProfile, 1, 0),
                TestCase(badProfile, 0, 1)
            ).forEachIndexed { index, testCase ->
                Then("#$index Should manage UI parts visibilities") {
                    every { useCase.execute(any(), captureLambda()) } answers {
                        lambda<(UseCaseResult<Profile>) -> Unit>().invoke(UseCaseResult.withResult(testCase.profile))
                    }
                    viewModel.loadProfileData()
                    verify(exactly = testCase.visibleCount) {
                        contactVisibilityObserver.onChanged(VISIBLE)
                        languagesVisibilityObserver.onChanged(VISIBLE)
                        socialsVisibilityObserver.onChanged(VISIBLE)
                    }
                    verify(exactly = testCase.goneCount) {
                        contactVisibilityObserver.onChanged(GONE)
                        languagesVisibilityObserver.onChanged(GONE)
                        socialsVisibilityObserver.onChanged(GONE)
                    }
                }
            }
            Then("Should provide visible UI parts") {
                val contactObserver = mockk<Observer<String>>()
                val languagesObserver = mockk<Observer<List<Language>>>()
                val socialsObserver = mockk<Observer<List<SocialLink>>>()
                val photoObserver = mockk<Observer<String>>()
                val aboutObserver = mockk<Observer<String>>()

                viewModel.contactCombined.observeForever(contactObserver)
                viewModel.languagesData.observeForever(languagesObserver)
                viewModel.socials.observeForever(socialsObserver)
                viewModel.profilePhoto.observeForever(photoObserver)
                viewModel.about.observeForever(aboutObserver)

                every { useCase.execute(any(), captureLambda()) } answers {
                    lambda<(UseCaseResult<Profile>) -> Unit>().invoke(UseCaseResult.withResult(goodProfile))
                }

                viewModel.loadProfileData()

                verify { contactObserver.onChanged("${goodContact.phoneNumber}\n${goodContact.email}") }
                verify { languagesObserver.onChanged(goodLanguages) }
                verify { socialsObserver.onChanged(goodSocials) }
                verify { photoObserver.onChanged(goodContact.photoUrl) }
                verify { aboutObserver.onChanged(goodProfile.about) }
            }
        }
        Scenario("Manage social link clicked") {
            val clickObserver = mockk<Observer<LiveDataAction<SocialLink>>>()
            viewModel.socialClick.observeForever(clickObserver)
            viewModel.onSocialClicked(goodSocials[0])

            val slot = CapturingSlot<LiveDataAction<SocialLink>>()
            verify { clickObserver.onChanged(capture(slot)) }
            assertEquals(goodSocials[0], slot.captured.value)
            assertNull(slot.captured.value)
        }
    }
})

private val goodLanguages
    get() = listOf(Language("language", 12.3f))
private val goodSocials
    get() = listOf(SocialLink("gl-social-link", ProfilePojo.SocialType.GITLAB))
private val goodContact
    get() = Contact("good-photo-url", "123-234-345", "email@post.pl", goodSocials)
private val goodProfile
    get() = Profile(goodContact, "about", goodLanguages)

private val badContact
    get() = Contact("", "", "", emptyList())
private val badProfile
    get() = Profile(badContact, "bad-about", emptyList())