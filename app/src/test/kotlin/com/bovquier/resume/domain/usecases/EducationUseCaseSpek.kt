package com.bovquier.resume.domain.usecases

import com.bovquier.resume.api.repositories.EducationRepository
import com.bovquier.resume.domain.usecases.education.EducationUseCase
import com.bovquier.resume.domain.usecases.education.EducationUseCaseParams
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.TestCoroutineDispatcher
import io.mockk.clearMocks
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.spekframework.spek2.style.gherkin.Feature

@ExperimentalCoroutinesApi
object EducationUseCaseSpek : BaseResumeSpek({
    Feature("EducationUseCaseSpek") {
        val repo = mockk<EducationRepository>()
        val useCase = EducationUseCase(repo, TestCoroutineDispatcher())

        beforeEachTest { clearMocks(repo) }

        Scenario("Executing success calls") {
            listOf(
                TestCaseSuccessExecute(ActionType.LOAD, true, 1),
                TestCaseSuccessExecute(ActionType.FOR_ERROR, false, 1),
                TestCaseSuccessExecute(ActionType.RETRIEVE, true, 0),
                TestCaseSuccessExecute(ActionType.RETRIEVE, false, 0)
            ).forEachIndexed { index, testCase ->
                Then("#$index: Should perform repo calls: ${testCase.repoCallsCount} times") {
                    useCase.execute(EducationUseCaseParams(testCase.actionType, "")) {}
                    coVerify(exactly = testCase.repoCallsCount) { repo.fetchEducation(any(), testCase.withCleaning) }
                }
            }
        }
    }
})

data class TestCaseSuccessExecute(val actionType: ActionType, val withCleaning: Boolean, val repoCallsCount: Int)