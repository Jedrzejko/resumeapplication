package com.bovquier.resume.domain.usecases.education

import com.bovquier.resume.api.repositories.EducationRepository
import com.bovquier.resume.setup.BaseResumeSpek
import com.bovquier.resume.setup.TestCoroutineDispatcher
import io.mockk.clearMocks
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.spekframework.spek2.style.gherkin.Feature

@ExperimentalCoroutinesApi
class EducationDetailsUseCaseSpek : BaseResumeSpek({
    Feature("EducationDetailsUseCaseSpek") {
        val repo = mockk<EducationRepository>()
        val useCase = EducationDetailsUseCase(repo, TestCoroutineDispatcher())

        beforeEachTest { clearMocks(repo) }

        Scenario("Executing success calls") {
            Then("Should perform repo call") {
                useCase.execute(123) {}
                coVerify(exactly = 1) { repo.loadEducation(123) }
            }
        }
    }
})