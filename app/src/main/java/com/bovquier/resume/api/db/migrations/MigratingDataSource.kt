package com.bovquier.resume.api.db.migrations

import android.content.Context
import com.bovquier.resume.common.secrets.Secrets
import io.requery.android.sqlcipher.SqlCipherDatabaseSource
import io.requery.meta.EntityModel
import net.sqlcipher.database.SQLiteDatabase

class MigratingDataSource(context: Context, model: EntityModel, version: Int, private val dbMigrations: DbMigrations) :
    SqlCipherDatabaseSource(context, model, Secrets.dbUserSecret, Secrets.dbKeySecret, version) {

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        val runner = AndroidDbMigrationRunner(db)
        dbMigrations.migrate(runner, oldVersion)
        super.onUpgrade(db, oldVersion, newVersion)
    }
}
