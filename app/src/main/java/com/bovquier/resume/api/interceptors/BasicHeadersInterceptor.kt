package com.bovquier.resume.api.interceptors

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class BasicHeadersInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder()
            .header("accept", "application/json")
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}