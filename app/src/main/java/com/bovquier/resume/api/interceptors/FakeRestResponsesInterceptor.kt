@file:Suppress("TooManyFunctions", "MagicNumber", "ComplexMethod")

package com.bovquier.resume.api.interceptors

import com.bovquier.resume.common.ResourceController
import com.bovquier.resume.common.secrets.Secrets
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import timber.log.Timber
import java.io.IOException
import java.nio.charset.StandardCharsets

class FakeRestResponsesInterceptor(private val resourceController: ResourceController) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val url = chain.request().url.toString()
        if (url.startsWith(Secrets.apiHostSecret)) Timber.d("Intercepting URL: $url")
        else return chain.proceed(chain.request())
        return try {
            when {
                urlForProfile(url) -> getProfileResponse(chain)
                urlForFramework(url) -> delayedRequest { getFrameworkResponse(chain) }
                urlForExperience(url) -> getExperienceResponse(chain)
                urlForEducation(url) -> getEducationResponse(chain)
                urlForErrorResponse(url) -> getErrorResponse(chain)
                else -> chain.proceed(chain.request())
            }
        } catch (e: IOException) {
            getErrorResponse(chain)
        }
    }

    private fun urlForErrorResponse(url: String) = url.matches(Regex(".*/fake.path"))
    private fun urlForProfile(url: String) = url.matches(Regex(".*/profile.json"))
    private fun urlForFramework(url: String) = url.matches(Regex(".*/framework.json"))
    private fun urlForExperience(url: String) = url.matches(Regex(".*/experience.json"))
    private fun urlForEducation(url: String) = url.matches(Regex(".*/education.json"))

    private fun getProfileResponse(chain: Chain): Response = loadJSONFromAsset("profile.json").toResponseBody("application/json".toMediaTypeOrNull())
        .let { ResponseBuilder.buildSuccessBody(it, chain) }

    private fun getFrameworkResponse(chain: Chain): Response = loadJSONFromAsset("framework.json").toResponseBody("application/json".toMediaTypeOrNull())
        .let { ResponseBuilder.buildSuccessBody(it, chain) }

    private fun getExperienceResponse(chain: Chain): Response = loadJSONFromAsset("experience.json").toResponseBody("application/json".toMediaTypeOrNull())
        .let { ResponseBuilder.buildSuccessBody(it, chain) }

    private fun getEducationResponse(chain: Chain): Response = loadJSONFromAsset("education.json").toResponseBody("application/json".toMediaTypeOrNull())
        .let { ResponseBuilder.buildSuccessBody(it, chain) }

    private fun getErrorResponse(chain: Chain): Response = errorResponse.toResponseBody("application/json".toMediaTypeOrNull())
        .let { ResponseBuilder.buildErrorResponse(it, chain, "Data are gone") }

    private fun <OutputType> delayedRequest(delay: Long = 750L, block: suspend () -> OutputType): OutputType = runBlocking {
        withContext(Dispatchers.Default) {
            delay(delay)
            block.invoke()
        }
    }

    private fun loadJSONFromAsset(fileName: String): String {
        val json: String
        try {
            val assetStream = resourceController.open(fileName)
            val size = assetStream.available()
            val buffer = ByteArray(size)
            assetStream.read(buffer)
            assetStream.close()
            json = String(buffer, StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return ""
        }
        return json
    }
}

object ResponseBuilder {
    fun buildErrorResponse(body: ResponseBody, chain: Chain, message: String, code: Int = 403): Response = Response.Builder()
        .request(chain.request()).message(message)
        .body(body)
        .protocol(Protocol.HTTP_1_1)
        .code(code)
        .build()

    fun buildSuccessBody(body: ResponseBody, chain: Chain): Response = Response.Builder()
        .body(body)
        .request(chain.request()).message("OK")
        .protocol(Protocol.HTTP_1_1)
        .code(200)
        .build()
}

private val errorResponse
    get() =
        """
            {
               "error_id":123,
               "error":"WRONG_PATH",
               "message":"Provided data has been destroyed"
            }
        """.trimIndent()
