package com.bovquier.resume.api.connectors

import com.bovquier.resume.data.pojo.CommonListResponse
import com.bovquier.resume.data.pojo.EducationPojo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface EducationConnector {
    @GET("{fileName}")
    suspend fun fetchEducation(@Path("fileName") fileName: String): Response<CommonListResponse<EducationPojo.EducationItem>>
}
