package com.bovquier.resume.api.repositories

import com.bovquier.resume.api.connectors.ExperienceConnector
import com.bovquier.resume.api.db.DatabaseController
import com.bovquier.resume.base.api.repositories.BaseRepository
import com.bovquier.resume.data.db.ExperienceEntities
import com.bovquier.resume.data.db.IExperience

class ExperienceRepository(private val connector: ExperienceConnector, private val dbController: DatabaseController) : BaseRepository() {
    @Suppress("ComplexRedundantLet")
    suspend fun fetchExperience(path: String): List<IExperience> = dbController.clearItems(IExperience::class)
        .let { performSimpleApiCall { connector.fetchExperience(path) } }
        .let { ExperienceEntities.mapResponse(it) }
        .let { dbController.saveEntities(it) }

    suspend fun loadExperience(experienceId: Int): IExperience = dbController.retrieveExperience(experienceId)
}
