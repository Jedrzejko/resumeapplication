package com.bovquier.resume.api.connectors

import com.bovquier.resume.data.pojo.ProfilePojo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ProfileConnector {
    @GET("{fileName}")
    suspend fun fetchProfileData(@Path("fileName") requestPath: String): Response<ProfilePojo.Profile>
}
