package com.bovquier.resume.api.db.converters

import io.requery.Converter
import java.util.*

class DateConverter : Converter<Date, Long> {
    fun convertToMapped(value: Long?) = convertToMapped(Date::class.java, value)

    override fun convertToMapped(type: Class<out Date>?, value: Long?): Date? = value?.let { dateValue ->
        if (dateValue == 0L) null
        else Date(dateValue)
    }

    override fun convertToPersisted(value: Date?): Long? = value?.time

    override fun getPersistedType(): Class<Long> = Long::class.java

    override fun getMappedType(): Class<Date> = Date::class.java

    override fun getPersistedSize(): Int? = null
}