package com.bovquier.resume.api.di

import android.content.Context
import com.bovquier.resume.api.db.DataStore
import com.bovquier.resume.api.db.DatabaseController
import com.bovquier.resume.api.db.converters.DateConverter
import io.requery.Persistable
import io.requery.sql.KotlinEntityDataStore
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single { DatabaseController(getDatabase(get()), getDatabaseDispatcher()) }
    single { buildDataStore(androidContext()) }
}

private fun getDatabaseDispatcher(): CoroutineDispatcher = Dispatchers.IO

private fun getDatabase(store: DataStore): KotlinEntityDataStore<Persistable> = store.kotlinStore

private fun buildDataStore(androidContext: Context): DataStore = DataStore.Builder(androidContext)
    .converters(requeryConverters)
    .build()

val requeryConverters
    get() = setOf(
        DateConverter()
    )
