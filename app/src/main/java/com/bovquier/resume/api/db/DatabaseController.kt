@file:Suppress("DEPRECATION", "TooManyFunctions")

package com.bovquier.resume.api.db

import com.bovquier.resume.common.CommonExceptions.MissingDatabaseItem
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.data.db.IExperience
import io.requery.Persistable
import io.requery.kotlin.BlockingEntityStore
import io.requery.query.Result
import io.requery.sql.KotlinEntityDataStore
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.withContext
import kotlin.reflect.KClass

class DatabaseController(
    private val database: KotlinEntityDataStore<Persistable>,
    private val dbDispatcher: CoroutineDispatcher
) {
    private val dbJob = Job()

    @Throws(MissingDatabaseItem::class)
    private suspend fun <T> invokeDatabaseOperations(block: BlockingEntityStore<Persistable>.() -> T) = withContext(dbDispatcher + dbJob) {
        database.invoke { block() }
    }

    // tmp helper for kotlin incompatibilities with requery - remove whole class when migrated to Room
    private fun <E> Result<E>.safeFirst(predicate: (E) -> Boolean): E = this.toList().firstOrNull { predicate(it) } ?: throw MissingDatabaseItem()

    suspend fun <T : Persistable> clearItems(kClass: KClass<T>): Int = invokeDatabaseOperations {
        delete(kClass).get().value()
    }

    suspend fun <T : Persistable> saveEntity(entity: T): T = invokeDatabaseOperations { upsert(entity) }

    suspend fun <T : Persistable> saveEntities(entities: List<T>): List<T> = invokeDatabaseOperations { upsert(entities).toList() }

    suspend fun retrieveEducation(educationId: Int): IEducation = invokeDatabaseOperations {
        select(IEducation::class)
//            .where(IEducation::educationId.eq(educationId)) do not use - remove requery, has problems with newer kotlin
            .get()
            .safeFirst { it.educationId == educationId }
    }

    suspend fun retrieveExperience(experienceId: Int): IExperience = invokeDatabaseOperations {
        select(IExperience::class)
//            .where(IExperience::experienceId.eq(experienceId)) do not use - remove requery, has problems with newer kotlin
            .get().safeFirst { it.experienceId == experienceId }
    }
}
