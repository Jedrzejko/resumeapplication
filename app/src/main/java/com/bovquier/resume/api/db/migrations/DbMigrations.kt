package com.bovquier.resume.api.db.migrations

import timber.log.Timber

class DbMigrations {

    fun migrate(statementRunner: StatementRunner, oldVersion: Int) {
        Timber.d("Running migrations on list: ${allMigrations.map { it::class.simpleName }}")
        for (dbMigration in allMigrations) {
            if (dbMigration.versionToMigrate > oldVersion) {
                dbMigration.migrate(statementRunner)
            }
        }
    }

    interface StatementRunner {
        fun runStatement(sql: String)
    }

    companion object {
        private var allMigrations = emptyArray<DbMigration>()
        val SCHEMA_VERSION: Int? = allMigrations.lastOrNull()?.versionToMigrate
    }
}
