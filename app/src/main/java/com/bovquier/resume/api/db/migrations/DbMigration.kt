package com.bovquier.resume.api.db.migrations

interface DbMigration {

    val versionToMigrate: Int

    fun migrate(statementRunner: DbMigrations.StatementRunner)
}
