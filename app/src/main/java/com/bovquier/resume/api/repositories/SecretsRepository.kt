package com.bovquier.resume.api.repositories

import com.bovquier.resume.base.api.repositories.BaseRepository
import com.bovquier.resume.data.pojo.SecretsData
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.secretmanager.v1.SecretManagerServiceClient
import com.google.cloud.secretmanager.v1.SecretManagerServiceSettings
import com.google.cloud.secretmanager.v1.SecretVersionName
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.ByteArrayInputStream
import java.io.IOException

class SecretsRepository(private val ioDispatcher: CoroutineDispatcher) : BaseRepository() {
    suspend fun fetchSecrets(responseListener: (SecretsData) -> Unit) = withContext(ioDispatcher) {
        try {
            FirebaseStorage.getInstance("gs://bqr-sss.appspot.com")
                .reference.child("bqr-sss-01d8a4a4dfbc.json")
                .getBytes(Long.MAX_VALUE)
                .addOnSuccessListener {
                    val credentialsInputStream = ByteArrayInputStream(it)
                    val credentials = GoogleCredentials.fromStream(credentialsInputStream)

                    val settings = SecretManagerServiceSettings
                        .newBuilder()
                        .setCredentialsProvider { credentials }.build()

                    val client: SecretManagerServiceClient = SecretManagerServiceClient.create(settings)
                    val apiBaseHost = SecretVersionName.of("bqr-sss", "resume-api-base-host", "1")
                    val apiHostSecret = client.accessSecretVersion(apiBaseHost)

                    val dbEncryptionKey = SecretVersionName.of("bqr-sss", "resume-db-encryption-key", "1")
                    val dbEncryptionKeySecret = client.accessSecretVersion(dbEncryptionKey)

                    val dbEncryptionUser = SecretVersionName.of("bqr-sss", "resume-db-encryption-user", "1")
                    val dbEncryptionUserSecret = client.accessSecretVersion(dbEncryptionUser)

                    val secrets = SecretsData(
                        dbEncryptionKeySecret.payload.data.toStringUtf8(),
                        dbEncryptionUserSecret.payload.data.toStringUtf8(),
                        apiHostSecret.payload.data.toStringUtf8()
                    )
                    responseListener.invoke(secrets)
                }.addOnFailureListener {
                    responseListener.invoke(SecretsData.empty())
                }
        } catch (e: IOException) {
            Timber.e(e, "Secrets not fetched")
            responseListener.invoke(SecretsData.empty())
        }
    }

}
