package com.bovquier.resume.api.repositories

import com.bovquier.resume.api.connectors.EducationConnector
import com.bovquier.resume.api.db.DatabaseController
import com.bovquier.resume.base.api.repositories.BaseRepository
import com.bovquier.resume.data.db.EducationEntities
import com.bovquier.resume.data.db.IEducation

class EducationRepository(private val educationConnector: EducationConnector, private val dbController: DatabaseController) : BaseRepository() {
    suspend fun fetchEducation(fileName: String, withClear: Boolean = true): List<IEducation> {
        if (withClear) dbController.clearItems(IEducation::class)
        return performSimpleApiCall { educationConnector.fetchEducation(fileName) }
            .let { EducationEntities.mapFromResponse(it) }
            .let { dbController.saveEntities(it) }
    }

    suspend fun loadEducation(educationId: Int): IEducation = dbController.retrieveEducation(educationId)
}
