package com.bovquier.resume.api.di

import com.bovquier.resume.api.connectors.EducationConnector
import com.bovquier.resume.api.connectors.ExperienceConnector
import com.bovquier.resume.api.connectors.ProfileConnector
import com.bovquier.resume.api.interceptors.BasicHeadersInterceptor
import com.bovquier.resume.api.interceptors.FakeRestResponsesInterceptor
import com.bovquier.resume.common.secrets.Secrets
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.internal.platform.Platform
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val clientModules = module {
    factory(named(INTERCEPTOR_FAKE_REST_RESPONSES)) { FakeRestResponsesInterceptor(get()) as Interceptor }
    factory(named(INTERCEPTOR_PRETTY_LOGGING)) { buildPrettyLoggingInterceptor() }
    factory(named(INTERCEPTOR_BASIC_HEADERS)) { BasicHeadersInterceptor() as Interceptor }
    factory(named(API_CLIENT)) {
        buildOkHttpClient(
            arrayOf(
                get(named(INTERCEPTOR_FAKE_REST_RESPONSES)),
                get(named(INTERCEPTOR_PRETTY_LOGGING)),
                get(named(INTERCEPTOR_BASIC_HEADERS))
            )
        )
    }
}

val connectorsModule = module {
    factory { getProfileConnector(buildRetrofit(get(named(API_CLIENT)))) }
    factory { getEducationConnector(buildRetrofit(get(named(API_CLIENT)))) }
    factory { getExperienceConnector(buildRetrofit(get(named(API_CLIENT)))) }
}

private fun getProfileConnector(retrofit: Retrofit): ProfileConnector = retrofit.create(ProfileConnector::class.java)
private fun getEducationConnector(retrofit: Retrofit): EducationConnector = retrofit.create(EducationConnector::class.java)
private fun getExperienceConnector(retrofit: Retrofit): ExperienceConnector = retrofit.create(ExperienceConnector::class.java)

private fun buildRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(Secrets.apiHostSecret)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}

private fun buildOkHttpClient(
    interceptors: Array<Interceptor>
): OkHttpClient {
    val builder = OkHttpClient.Builder()
    interceptors.forEach { builder.addInterceptor(it) }
    return builder.build()
}

private fun buildPrettyLoggingInterceptor(): Interceptor = LoggingInterceptor.Builder()
//    .loggable(BuildConfig.DEBUG)
    .setLevel(Level.BASIC)
    .log(Platform.INFO)
    .request("REQUEST")
    .response("RESPONSE")
    .build()


private const val INTERCEPTOR_FAKE_REST_RESPONSES: String = "module:api:INTERCEPTOR_FAKE_REST_RESPONSES"
private const val API_CLIENT: String = "module:api:API_CLIENT"
private const val INTERCEPTOR_PRETTY_LOGGING: String = "module:api:INTERCEPTOR_PRETTY_LOGGING"
private const val INTERCEPTOR_BASIC_HEADERS: String = "module:api:INTERCEPTOR_BASIC_HEADERS"