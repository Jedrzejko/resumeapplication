package com.bovquier.resume.api.connectors

import com.bovquier.resume.data.pojo.CommonListResponse
import com.bovquier.resume.data.pojo.ExperiencePojo.ExperienceItemPojo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ExperienceConnector {
    @GET("{fileName}")
    suspend fun fetchExperience(@Path("fileName") fileName: String): Response<CommonListResponse<ExperienceItemPojo>>
}
