package com.bovquier.resume.api.repositories

import com.bovquier.resume.api.connectors.ProfileConnector
import com.bovquier.resume.base.api.repositories.BaseRepository
import com.bovquier.resume.data.pojo.ProfilePojo
import com.bovquier.resume.domain.usecases.profile.ProfileUseCaseParams

class ProfileRepository(private val profileConnector: ProfileConnector) : BaseRepository() {
    suspend fun fetchProfile(profileUseCaseParams: ProfileUseCaseParams): ProfilePojo.Profile = performSimpleApiCall {
        profileConnector.fetchProfileData(profileUseCaseParams.requestPath)
    }
}
