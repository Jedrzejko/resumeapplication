package com.bovquier.resume.api.db

import android.content.Context
import com.bovquier.resume.api.db.migrations.DbMigrations
import com.bovquier.resume.api.db.migrations.DbMigrations.Companion.SCHEMA_VERSION
import com.bovquier.resume.api.db.migrations.MigratingDataSource
import com.bovquier.resume.data.db.Models
import io.requery.Converter
import io.requery.Persistable
import io.requery.android.DefaultMapping
import io.requery.cache.EmptyEntityCache
import io.requery.sql.Configuration
import io.requery.sql.ConfigurationBuilder
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.Platform
import io.requery.sql.SchemaModifier
import io.requery.sql.TableCreationMode
import io.requery.sql.platform.SQLite

class DataStore private constructor(
    private val configuration: Configuration,
    val kotlinStore: KotlinEntityDataStore<Persistable>
) {
    @Suppress("unused")
    fun recreateTables(): Long {
        val schemaModifier = SchemaModifier(configuration)
        schemaModifier.createTables(TableCreationMode.DROP_CREATE)
        return -1L
    }

    class Builder(context: Context) {

        private val platform: Platform = SQLite()
        private val configurationBuilder: ConfigurationBuilder

        fun converters(converters: Set<Converter<*, *>>): Builder {
            val defaultMapping = DefaultMapping(platform)

            for (converter in converters) {
                defaultMapping.addConverter(converter)
            }

            configurationBuilder.setMapping(defaultMapping)

            return this
        }

        fun build(): DataStore {
            val configuration = configurationBuilder.build()
            val kotlinDataStore = KotlinEntityDataStore<Persistable>(configuration)
            return DataStore(configuration, kotlinDataStore)
        }

        init {
            val source = MigratingDataSource(context, Models.DEFAULT, LATEST_DB_VERSION, DbMigrations())
            source.setTableCreationMode(TableCreationMode.CREATE_NOT_EXISTS)
            source.setLoggingEnabled(true)
            configurationBuilder = ConfigurationBuilder(source, Models.DEFAULT)
            configurationBuilder.setEntityCache(EmptyEntityCache())
        }

    }

    companion object {

        // Don't change this values.
        // It marks from which DB version we started to migrate the schema
        private const val INITIAL_DB_SCHEMA_VERSION = 1
        private val LATEST_DB_VERSION = SCHEMA_VERSION ?: INITIAL_DB_SCHEMA_VERSION
    }
}