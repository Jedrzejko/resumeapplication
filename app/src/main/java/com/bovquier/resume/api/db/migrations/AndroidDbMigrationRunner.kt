package com.bovquier.resume.api.db.migrations

import net.sqlcipher.database.SQLiteDatabase

class AndroidDbMigrationRunner(private val db: SQLiteDatabase) : DbMigrations.StatementRunner {

    override fun runStatement(sql: String) {
        db.execSQL(sql)
    }
}
