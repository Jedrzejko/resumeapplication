package com.bovquier.resume.data.pojo

import com.google.gson.annotations.SerializedName

data class ApiCallErrorPojo(
    @SerializedName("error_id") val errorId: Int,
    @SerializedName("error") val error: String,
    @SerializedName("message") val message: String
)
