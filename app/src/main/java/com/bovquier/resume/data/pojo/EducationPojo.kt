package com.bovquier.resume.data.pojo

import com.google.gson.annotations.SerializedName

object EducationPojo {

    data class EducationItem(
        @SerializedName("finish_year") val finishYear: String,
        @SerializedName("school") val school: String,
        @SerializedName("field_of_studies") val fieldOfStudies: String,
        @SerializedName("title") val title: String,
        @SerializedName("project") val projectItem: ProjectItem
    )

    data class ProjectItem(
        @SerializedName("project_name") val projectName: String,
        @SerializedName("project_description") val projectDescription: String
    )
}