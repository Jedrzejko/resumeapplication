package com.bovquier.resume.data.db

import com.bovquier.resume.data.db.EducationEntities.COL_LINK_EDUCATION_PROJECT
import com.bovquier.resume.data.pojo.CommonListResponse
import com.bovquier.resume.data.pojo.EducationPojo
import com.bovquier.resume.data.pojo.EducationPojo.EducationItem
import io.requery.Column
import io.requery.Entity
import io.requery.ForeignKey
import io.requery.Generated
import io.requery.Key
import io.requery.OneToOne
import io.requery.Persistable

@Entity
interface IEducation : Persistable {
    @get:Key
    @get:Generated
    val educationId: Int
    var finishYear: String
    var school: String
    var fos: String
    var title: String

    @get:ForeignKey
    @get:OneToOne(mappedBy = COL_LINK_EDUCATION_PROJECT)
    var project: IProject
}

@Entity
interface IProject : Persistable {
    @get:Key
    @get:Generated
    val projectId: Int
    var projectName: String
    var projectDescription: String

    @get:OneToOne
    @get:Column(name = COL_LINK_EDUCATION_PROJECT)
    var education: IEducation
}

object EducationEntities {
    const val COL_LINK_EDUCATION_PROJECT = "IEducation_IProject"

    fun mapFromResponse(response: CommonListResponse<EducationItem>) = response.rows.let { list -> list.map { mapEducationItem(it) } }

    private fun mapEducationItem(response: EducationItem): IEducation = Education().apply {
        finishYear = response.finishYear
        school = response.school
        fos = response.fieldOfStudies
        title = response.title
        project = mapFromResponse(response.projectItem)
    }

    private fun mapFromResponse(response: EducationPojo.ProjectItem): IProject = Project().apply {
        projectName = response.projectName
        projectDescription = response.projectDescription
    }
}