package com.bovquier.resume.data.pojo

import com.google.gson.annotations.SerializedName

object ExperiencePojo {
    data class ExperienceItemPojo(
        @SerializedName("job_title") val jobTitle: String,
        @SerializedName("job_start") val jobStart: Long,
        @SerializedName("job_end") val jobEnd: Long?,
        @SerializedName("company") val company: CompanyPojo,
        @SerializedName("job_city") val jobCity: String,
        @SerializedName("job_background") val jobBackground: JobBackgroundPojo
    )

    data class CompanyPojo(
        @SerializedName("company_name") val companyName: String,
        @SerializedName("company_website") val companyWebsite: String
    )

    data class JobBackgroundPojo(
        @SerializedName("description") val description: String,
        @SerializedName("used_frameworks") val usedFrameworks: List<String>
    )

    data class Framework(
        @SerializedName("name") val name: String,
        @SerializedName("level") val level: Float
    )
}