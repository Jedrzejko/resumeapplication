package com.bovquier.resume.data.pojo

/** Not exactly POJO data, but keep it in this package */
data class SecretsData(val dbKey: String, val dbUser: String, val apiHost: String) {
    companion object {
        fun empty() = SecretsData("", "", "")
    }
}