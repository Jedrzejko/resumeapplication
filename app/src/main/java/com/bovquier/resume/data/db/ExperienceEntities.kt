package com.bovquier.resume.data.db

import com.bovquier.resume.api.db.converters.DateConverter
import com.bovquier.resume.data.db.ExperienceEntities.COL_LINK_EXPERIENCE_COMPANY
import com.bovquier.resume.data.db.ExperienceEntities.COL_LINK_EXPERIENCE_JOB_BG
import com.bovquier.resume.data.db.ExperienceEntities.COL_LINK_JOB_BG_FRAMEWORK
import com.bovquier.resume.data.pojo.CommonListResponse
import com.bovquier.resume.data.pojo.ExperiencePojo.CompanyPojo
import com.bovquier.resume.data.pojo.ExperiencePojo.ExperienceItemPojo
import com.bovquier.resume.data.pojo.ExperiencePojo.JobBackgroundPojo
import io.requery.*
import java.util.*


@Entity
interface IExperience : Persistable {
    @get:Key
    @get:Generated
    val experienceId: Int
    var jobTitle: String
    var jobStart: Date
    var jobEnd: Date?
    var jobCity: String

    @get:ForeignKey
    @get:OneToOne(mappedBy = COL_LINK_EXPERIENCE_JOB_BG)
    var jobBackground: IJobBackground

    @get:ForeignKey
    @get:OneToOne(mappedBy = COL_LINK_EXPERIENCE_COMPANY)
    var company: ICompany
}

@Entity
interface ICompany : Persistable {
    @get:Key
    @get:Generated
    val companyId: Int
    var companyName: String
    var companyWebsite: String

    @get:OneToOne
    @get:Column(name = COL_LINK_EXPERIENCE_COMPANY)
    var experience: IExperience
}

@Entity
interface IJobBackground : Persistable {
    @get:Key
    @get:Generated
    val backgrouId: Int
    var description: String

    @get:OneToMany(mappedBy = COL_LINK_JOB_BG_FRAMEWORK)
    val usedFrameworks: MutableSet<IFramework>

    @get:OneToOne
    @get:Column(name = COL_LINK_EXPERIENCE_JOB_BG)
    var experience: IExperience
}

@Entity
interface IFramework : Persistable {
    @get:Key
    @get:Generated
    val frameworkId: Int
    var name: String

    @get:ManyToOne
    @get:JunctionTable
    @get:Column(name = COL_LINK_JOB_BG_FRAMEWORK)
    var experience: IJobBackground
}


object ExperienceEntities {
    const val COL_LINK_EXPERIENCE_JOB_BG = "IExperience_IJobBackground"
    const val COL_LINK_EXPERIENCE_COMPANY = "IExperience_ICompany"
    const val COL_LINK_JOB_BG_FRAMEWORK = "IJobBackground_IFramework"

    fun mapResponse(response: CommonListResponse<ExperienceItemPojo>): List<IExperience> = response.rows.map { mapExperienceItem(it) }

    private fun mapExperienceItem(response: ExperienceItemPojo): IExperience = Experience().apply {
        jobTitle = response.jobTitle
        jobStart = DateConverter().convertToMapped(response.jobStart)
        jobEnd = DateConverter().convertToMapped(response.jobEnd)
        company = mapCompanyItem(response.company)
        jobCity = response.jobCity
        jobBackground = mapBackgroundItem(response.jobBackground)
    }

    private fun mapBackgroundItem(response: JobBackgroundPojo): IJobBackground = JobBackground().apply {
        description = response.description
        usedFrameworks.addAll(response.usedFrameworks.map { mapFrameworkItem(it) })
    }

    private fun mapFrameworkItem(response: String): IFramework = Framework().apply {
        name = response
    }

    private fun mapCompanyItem(response: CompanyPojo): ICompany = Company().apply {
        companyName = response.companyName
        companyWebsite = response.companyWebsite
    }
}