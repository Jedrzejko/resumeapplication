package com.bovquier.resume.data.pojo

import com.google.gson.annotations.SerializedName

data class CommonListResponse<ResponseItem>(
    @SerializedName("rows") val rows: List<ResponseItem> = emptyList()
)