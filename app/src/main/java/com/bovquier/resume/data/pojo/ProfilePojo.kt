package com.bovquier.resume.data.pojo

import androidx.annotation.DrawableRes
import com.bovquier.resume.R
import com.google.gson.annotations.SerializedName

object ProfilePojo {
    data class Profile(
        @SerializedName("contact") val contact: Contact,
        @SerializedName("about") val about: String,
        @SerializedName("languages") val languages: List<Language>
    )

    data class Language(
        @SerializedName("language") val language: String,
        @SerializedName("language_level") val languageLevel: Float
    )

    data class Contact(
        @SerializedName("photo_url") val photoUrl: String,
        @SerializedName("phone_number") val phoneNumber: String,
        @SerializedName("email") val email: String,
        @SerializedName("social") val social: List<SocialLink>
    )

    data class SocialLink(
        @SerializedName("link") val link: String,
        @SerializedName("social_type") val socialType: SocialType
    )

    enum class SocialType(@DrawableRes val logoId: Int) {
        LINKED_IN(R.drawable.ic_linked_in),
        REMOTE(R.drawable.ic_remote),
        GITLAB(R.drawable.ic_gitlab)
    }
}