package com.bovquier.resume.common

import android.content.Context
import android.content.res.Resources
import android.widget.Toast
import androidx.annotation.StringRes

class ToastManager(private val context: Context, private val resourceController: ResourceController) {
    private lateinit var toast: Toast

    fun showLongToast(@StringRes stringResourceId: Int, extraStringParam: Any? = null) {
        showToast(extractString(stringResourceId, extraStringParam), Toast.LENGTH_LONG)
    }

    fun showShortToast(@StringRes stringResourceId: Int, extraStringParam: Any? = null) {
        showToast(extractString(stringResourceId, extraStringParam), Toast.LENGTH_SHORT)
    }

    fun showLongToast(msg: String) {
        showToast(msg, Toast.LENGTH_LONG)
    }

    fun showShortToast(msg: String) {
        showToast(msg, Toast.LENGTH_SHORT)
    }

    private fun extractString(stringResourceId: Int, extraStringParam: Any? = null): String =
        (if (extraStringParam == null) resourceController.getString(stringResourceId) else try {
            resourceController.getQuantityString(stringResourceId, extraStringParam as Int)
        } catch (@Suppress("TooGenericExceptionCaught") e: Exception) {
            when (e) {
                is Resources.NotFoundException -> resourceController.getString(stringResourceId, extraStringParam)
                is ClassCastException -> resourceController.getString(stringResourceId, extraStringParam)
                else -> resourceController.getString(stringResourceId)
            }
        }).toString()

    private fun showToast(string: String, duration: Int) {
        cancelToast()
        toast = Toast.makeText(context, string, duration)
        toast.show()
    }

    private fun cancelToast() {
        if (this::toast.isInitialized) toast.cancel()
    }

}
