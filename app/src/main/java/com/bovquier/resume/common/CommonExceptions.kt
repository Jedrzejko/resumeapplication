package com.bovquier.resume.common

import com.bovquier.resume.data.pojo.ApiCallErrorPojo
import com.google.gson.Gson
import okhttp3.ResponseBody

object CommonExceptions {
    class MissingConfigException(msg: String) : Throwable(msg)
    class MissingDatabaseItem : Throwable()
    class ApiCallException private constructor(val errorPojo: ApiCallErrorPojo? = null) : Throwable() {
        class Builder(private val errorBody: ResponseBody?) {
            fun build(): ApiCallException = errorBody
                ?.let { Gson().fromJson(errorBody.string(), ApiCallErrorPojo::class.java) }
                ?.let { ApiCallException(it) }
                ?: ApiCallException()
        }
    }
}
