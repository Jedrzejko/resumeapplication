package com.bovquier.resume.common

import java.util.concurrent.atomic.AtomicBoolean

class LiveDataAction<out T>(private val content: T) {

    private val handled = AtomicBoolean(false)

    val value: T?
        get() = if (handled.compareAndSet(false, true)) content else null
}