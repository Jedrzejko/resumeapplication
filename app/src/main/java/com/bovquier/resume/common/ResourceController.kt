package com.bovquier.resume.common

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.PluralsRes
import androidx.core.content.ContextCompat
import com.bovquier.resume.R
import java.io.InputStream

class ResourceController(private val context: Context) {
    fun open(fileName: String): InputStream = context.assets.open(fileName)

    fun getString(stringId: Int): CharSequence = context.getString(stringId)

    fun getString(resId: Int, vararg formatArgs: Any): CharSequence = context.getString(resId, *formatArgs)

    fun getQuantityString(@PluralsRes pluralStringId: Int, quantity: Int): CharSequence =
        context.resources.getQuantityString(pluralStringId, quantity, quantity)

    fun getSocialDrawable(@DrawableRes drawableId: Int?): Drawable? = drawableId?.let { getDrawable(it) } ?: getDrawable(R.drawable.placeholder)

    private fun getDrawable(it: Int) = ContextCompat.getDrawable(context, it)
}