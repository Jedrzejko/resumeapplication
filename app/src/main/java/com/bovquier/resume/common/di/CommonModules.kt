package com.bovquier.resume.common.di

import com.bovquier.resume.common.MainCoroutineDispatcher
import com.bovquier.resume.common.ResourceController
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.common.ToastManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val commonUtilsModule = module {
    factory { MainCoroutineDispatcher() as ResumeCoroutineDispatcher }
    factory { ResourceController(androidContext()) }
    factory { ToastManager(androidContext(), get()) }
}