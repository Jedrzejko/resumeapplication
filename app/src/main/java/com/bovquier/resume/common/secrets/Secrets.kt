package com.bovquier.resume.common.secrets

import com.bovquier.resume.common.CommonExceptions
import com.bovquier.resume.data.pojo.SecretsData

object Secrets {

    var apiHostSecret: String = ""
        get() = field.ifBlank { throw CommonExceptions.MissingConfigException("API host not defined") }
        private set

    var dbUserSecret: String = ""
        get() = field.ifBlank { throw CommonExceptions.MissingConfigException("DB User not defined") }
        private set

    var dbKeySecret: String = ""
        get() = field.ifBlank { throw CommonExceptions.MissingConfigException("DB Key not defined") }
        private set

    fun set(secrets: SecretsData) {
        apiHostSecret = secrets.apiHost
        dbUserSecret = secrets.dbUser
        dbKeySecret = secrets.dbKey
    }

}
