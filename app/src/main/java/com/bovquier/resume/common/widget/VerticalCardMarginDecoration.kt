package com.bovquier.resume.common.widget

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class VerticalCardMarginDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                top = spaceHeight
            }
            bottom = spaceHeight
        }
    }
}

class HorizontalCardMarginDecoration(private val spacingWidth: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                left = spacingWidth
            }
            right = spacingWidth
        }
    }
}

class GridSpacingDecorator(private val spacingWidth: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            val position = parent.getChildAdapterPosition(view)
            top = if (position < 3) {
                0
            } else {
                spacingWidth
            }
            if (position % 3 == 0) {
                outRect.left = 0
                outRect.right = spacingWidth
            } else if ((position + 1) % 3 == 0) {
                outRect.right = 0
                outRect.left = spacingWidth
            }
        }
    }
}
