package com.bovquier.resume.common

import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

class MainCoroutineDispatcher : ResumeCoroutineDispatcher {
    override val bgContext: CoroutineContext
        get() = Dispatchers.IO
    override val fgContext: CoroutineContext
        get() = Dispatchers.Main
}

interface ResumeCoroutineDispatcher {
    val bgContext: CoroutineContext
    val fgContext: CoroutineContext
}
