package com.bovquier.resume.common

import java.util.regex.Pattern

fun String?.getWithDefault(default: String = ""): String = if (this?.isBlank() != false) default else this

/** After {@link Patterns.EMAIL_ADDRESS} - unable to use in jUnits */
val emailPattern: Pattern = Pattern.compile(
    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
)


fun <E> List<E>?.withNotEmpty(withAction: (List<E>) -> Unit) {
    if (this != null && isNotEmpty()) withAction(this)
}