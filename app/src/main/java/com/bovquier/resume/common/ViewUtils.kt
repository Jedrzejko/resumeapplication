package com.bovquier.resume.common

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

@Suppress("UNCHECKED_CAST")
fun <T : Any> Fragment.argument(key: String, default: T) = lazy {
    arguments?.get(key) as? T ?: default
}

@Suppress("UNCHECKED_CAST")
fun <T : Any> Fragment.argument(key: String) = lazy {
    arguments?.get(key) as? T
}

@Suppress("unused")
fun hideKeyboard(view: View) {
    if (view.context != null) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun hideKeyboard(hostActivity: Activity) {
    val imm = hostActivity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(
        hostActivity.findViewById<View>(android.R.id.content).rootView.windowToken,
        InputMethodManager.HIDE_NOT_ALWAYS
    )
    imm.hideSoftInputFromWindow(hostActivity.findViewById<View>(android.R.id.content).rootView.windowToken, 0)
}

fun <T> LiveData<LiveDataAction<T>>.observe(owner: LifecycleOwner, action: (T) -> Unit) {
    observe(owner, Observer { actionData -> actionData.value?.let { action(it) } })
}