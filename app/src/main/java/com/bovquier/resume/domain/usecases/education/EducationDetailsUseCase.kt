package com.bovquier.resume.domain.usecases.education

import com.bovquier.resume.api.repositories.EducationRepository
import com.bovquier.resume.base.domain.usecases.BaseUseCase
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.data.db.IEducation

class EducationDetailsUseCase(private val educationRepository: EducationRepository, dispatchers: ResumeCoroutineDispatcher) : BaseUseCase<Int, IEducation>(dispatchers) {
    override suspend fun executeOnBackground(params: Int): IEducation = educationRepository.loadEducation(params)

}
