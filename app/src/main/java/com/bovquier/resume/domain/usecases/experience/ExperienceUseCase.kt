package com.bovquier.resume.domain.usecases.experience

import com.bovquier.resume.api.repositories.ExperienceRepository
import com.bovquier.resume.base.domain.usecases.BaseUseCase
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.data.db.IExperience

class ExperienceUseCase(private val repository: ExperienceRepository, dispatchers: ResumeCoroutineDispatcher) :
    BaseUseCase<String, List<IExperience>>(dispatchers) {
    override suspend fun executeOnBackground(params: String): List<IExperience> = repository.fetchExperience(params)
}