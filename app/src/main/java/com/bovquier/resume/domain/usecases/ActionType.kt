package com.bovquier.resume.domain.usecases

enum class ActionType {
    LOAD,
    RETRIEVE,
    FOR_ERROR
}