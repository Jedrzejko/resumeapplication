package com.bovquier.resume.domain.usecases.main

import com.bovquier.resume.api.repositories.SecretsRepository
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.common.secrets.Secrets
import com.bovquier.resume.data.pojo.SecretsData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class SecretsUseCase(private val secretsRepository: SecretsRepository, private val dispatchers: ResumeCoroutineDispatcher) {
    fun fetchSecrets(resultListener: (SecretsState) -> Unit) = CoroutineScope(dispatchers.fgContext).launch {
        try {
            secretsRepository.fetchSecrets { secretsData ->
                val result = if (secretsData.isEmpty()) {
                    SecretsState.SecretsError
                } else {
                    Secrets.set(secretsData)
                    SecretsState.SecretsRetrieved
                }
                resultListener.invoke(result)
            }
        } catch (e: Exception) {
            resultListener.invoke(SecretsState.SecretsError)
        }
    }

    private fun SecretsData.isEmpty(): Boolean = this.apiHost.isBlank() || this.dbKey.isBlank() || this.dbUser.isBlank()

    sealed class SecretsState {
        object SecretsRetrieved : SecretsState()
        object SecretsError : SecretsState()
    }
}
