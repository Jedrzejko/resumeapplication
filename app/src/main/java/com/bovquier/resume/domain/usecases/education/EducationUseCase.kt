package com.bovquier.resume.domain.usecases.education

import com.bovquier.resume.api.repositories.EducationRepository
import com.bovquier.resume.base.domain.usecases.BaseUseCase
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.domain.usecases.ActionType
import com.bovquier.resume.domain.usecases.ActionType.FOR_ERROR
import com.bovquier.resume.domain.usecases.ActionType.LOAD

class EducationUseCase(
    private val educationRepository: EducationRepository,
    dispatcherProvider: ResumeCoroutineDispatcher
) : BaseUseCase<EducationUseCaseParams, List<IEducation>>(dispatcherProvider) {
    override suspend fun executeOnBackground(params: EducationUseCaseParams): List<IEducation> = when (params.actionType) {
        LOAD -> educationRepository.fetchEducation(params.loadPath)
        FOR_ERROR -> educationRepository.fetchEducation(params.loadPath, false)
        else -> emptyList()
    }
}

data class EducationUseCaseParams(val actionType: ActionType = LOAD, val loadPath: String)
