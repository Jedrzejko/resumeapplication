package com.bovquier.resume.domain.usecases.profile

import com.bovquier.resume.api.repositories.ProfileRepository
import com.bovquier.resume.base.domain.usecases.BaseUseCase
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.data.pojo.ProfilePojo.Profile
import com.bovquier.resume.domain.usecases.ActionType

class ProfileUseCase(private val profileRepository: ProfileRepository, dispatchers: ResumeCoroutineDispatcher) : BaseUseCase<ProfileUseCaseParams, Profile>(dispatchers) {
    override suspend fun executeOnBackground(params: ProfileUseCaseParams): Profile = profileRepository.fetchProfile(params)
}

data class ProfileUseCaseParams(val actionType: ActionType = ActionType.LOAD, val requestPath: String)