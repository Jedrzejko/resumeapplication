package com.bovquier.resume.domain.usecases.experience

import com.bovquier.resume.api.repositories.ExperienceRepository
import com.bovquier.resume.base.domain.usecases.BaseUseCase
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import com.bovquier.resume.data.db.IExperience

class ExperienceDetailsUseCase(private val experienceRepository: ExperienceRepository, dispatcher: ResumeCoroutineDispatcher) : BaseUseCase<Int, IExperience>(dispatcher) {
    override suspend fun executeOnBackground(params: Int): IExperience = experienceRepository.loadExperience(params)

}
