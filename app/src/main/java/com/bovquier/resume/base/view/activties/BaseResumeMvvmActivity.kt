package com.bovquier.resume.base.view.activties

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.bovquier.resume.BR
import com.bovquier.resume.base.view.contract.MvvmDatabindingContract
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.common.ToastManager
import com.bovquier.resume.common.observe
import org.koin.android.ext.android.inject

abstract class BaseResumeMvvmActivity<BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel>
    : AppCompatActivity(),
    MvvmDatabindingContract<BINDING, VIEW_MODEL> {

    private val toastManager by inject<ToastManager>()

    abstract val layoutId: Int
    override lateinit var binding: BINDING

    final override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = prepareViewDataBinding()
        setContentView(binding.root)
        binding.lifecycleOwner = this
        binding.setVariable(BR.viewModel, viewViewModel)
        observeBaseVmData()
        observeViewModelData(this)
        postCreate()
    }

    private fun prepareViewDataBinding(): BINDING = DataBindingUtil.inflate(layoutInflater, layoutId, null, false)

    private fun observeBaseVmData() {
        viewViewModel.forceBinding.observe(this, Observer { binding.executePendingBindings() })
        viewViewModel.toastResData.observe(this) { toastManager.showLongToast(it) }
        viewViewModel.toastStrData.observe(this) { toastManager.showLongToast(it) }
    }
}