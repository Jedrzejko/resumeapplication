package com.bovquier.resume.base.view.contract

import androidx.databinding.ViewDataBinding
import com.bovquier.resume.base.view.viewmodels.BaseViewModel

interface MvvmDatabindingContract<BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel> :
    MvvmContract<VIEW_MODEL> {
    var binding: BINDING
}