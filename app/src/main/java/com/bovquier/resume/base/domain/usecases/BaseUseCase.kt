package com.bovquier.resume.base.domain.usecases

import com.bovquier.resume.common.CommonExceptions.MissingDatabaseItem
import com.bovquier.resume.common.ResumeCoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class BaseUseCase<InputType, OutputType>(private val dispatchers: ResumeCoroutineDispatcher) {

    private var parentJob: Job = Job()

    protected abstract suspend fun executeOnBackground(params: InputType): OutputType

    /**
     * Manages to perform bg operation like DB queries
     */
    fun execute(params: InputType, onComplete: (UseCaseResult<OutputType>) -> Unit) {
        unsubscribe()
        parentJob = Job()
        CoroutineScope(dispatchers.fgContext + parentJob).launch {
            try {
                val result = withContext(dispatchers.bgContext) {
                    executeOnBackground(params)
                }
                onComplete.invoke(UseCaseResult.withResult(result = result))
            } catch (mdb: MissingDatabaseItem) {
                onComplete.invoke(UseCaseResult.withError(mdb))
            } catch (@Suppress("TooGenericExceptionCaught") t: Throwable) {
                onComplete.invoke(UseCaseResult.withError(error = t))
            }
        }
    }

    fun execute(params: InputType, onError: (Throwable) -> Unit, onSuccess: (OutputType) -> Unit) = execute(params) {
        it.result?.also(onSuccess)
        it.error?.also(onError)
    }

    private fun unsubscribe() {
        parentJob.cancel()
    }
}

class UseCaseResult<out OutputType> private constructor(val result: OutputType? = null, val error: Throwable? = null) {
    companion object {
        fun <OutputType> withResult(result: OutputType) = UseCaseResult(result = result)
        fun <OutputType> withError(error: Throwable) = UseCaseResult<OutputType>(error = error)
    }
}