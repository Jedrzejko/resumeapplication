package com.bovquier.resume.base.view.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bovquier.resume.BR
import com.bovquier.resume.base.view.activties.BaseResumeMvvmActivity
import com.bovquier.resume.base.view.contract.MvvmDatabindingContract
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.common.ToastManager
import com.bovquier.resume.common.observe
import org.koin.android.ext.android.inject
import org.koin.core.scope.Scope

abstract class BaseResumeMvvmFragment<BINDING : ViewDataBinding, VIEW_MODEL : BaseViewModel> : Fragment(),
    MvvmDatabindingContract<BINDING, VIEW_MODEL> {
    abstract val layoutId: Int
    abstract val viewScope: Scope
    override lateinit var binding: BINDING

    private val toastManager by inject<ToastManager>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        require(context is BaseResumeMvvmActivity<*, *>) { "Created fragment must be attached to subclass of ${BaseResumeMvvmActivity::class.java.simpleName}" }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeBaseVmData()
    }

    private fun observeBaseVmData() {
        viewViewModel.forceBinding.observe(viewLifecycleOwner, Observer { binding.executePendingBindings() })
        viewViewModel.toastResData.observe(viewLifecycleOwner) { toastManager.showLongToast(it) }
        viewViewModel.toastStrData.observe(viewLifecycleOwner) { toastManager.showLongToast(it) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.setVariable(BR.viewModel, viewViewModel)
        return binding.root
    }

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModelData(viewLifecycleOwner)
        postCreate()
    }
}
