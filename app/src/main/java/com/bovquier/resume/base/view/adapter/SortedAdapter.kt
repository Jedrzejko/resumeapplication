package com.bovquier.resume.base.view.adapter

import androidx.recyclerview.widget.SortedList
import com.bovquier.resume.R
import com.bovquier.resume.databinding.ItemClickableBasicBinding

abstract class SortedAdapter<TYPE : Any, VH : ListingAdapterViewHolder<TYPE>> : BindableAdapter<TYPE, ItemClickableBasicBinding, VH, SortedList<TYPE>>() {
    override val layoutId: Int = R.layout.item_clickable_basic
    override fun getItem(position: Int): TYPE = items.get(position)
    override fun getItemCount(): Int = items.size()
    override fun putItems(list: List<TYPE>?) {
        list?.also { items.addAll(list) }
    }
}

abstract class ListingAdapterViewHolder<TYPE>(binding: ItemClickableBasicBinding) : AdapterItemViewHolder<ItemClickableBasicBinding, TYPE>(binding)