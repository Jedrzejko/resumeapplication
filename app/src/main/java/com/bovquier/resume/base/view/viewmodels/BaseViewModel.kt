package com.bovquier.resume.base.view.viewmodels

import androidx.annotation.StringRes
import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bovquier.resume.R
import com.bovquier.resume.base.domain.usecases.UseCaseResult
import com.bovquier.resume.common.CommonExceptions.ApiCallException
import com.bovquier.resume.common.CommonExceptions.MissingDatabaseItem
import com.bovquier.resume.common.LiveDataAction
import timber.log.Timber

abstract class BaseViewModel : BindingViewModel(), Observable {

    private val toastManagerData: MutableLiveData<Int> = MutableLiveData()
    val toastResData: LiveData<LiveDataAction<Int>> = Transformations.map(toastManagerData) { LiveDataAction(it) }
    private val toastManagerStringData: MutableLiveData<String> = MutableLiveData()
    val toastStrData: LiveData<LiveDataAction<String>> = Transformations.map(toastManagerStringData) { LiveDataAction(it) }

    protected fun handleError(error: Throwable?) {
        when (error) {
            is MissingDatabaseItem -> showMissingItem().run { Timber.d(error) }
            is ApiCallException -> handleApiException(error)
            else -> Timber.e(error, error?.localizedMessage ?: error.toString())
        }
    }

    private fun handleApiException(apiCallException: ApiCallException) {
        apiCallException.errorPojo?.also { callError ->
            when (callError.errorId) {
                123 -> showMessage(callError.message)
                else -> showMessage("Unknown error code: ${callError.error}(${callError.errorId})")
            }
        }
    }

    private fun showMissingItem() {
        toastManagerData.postValue(R.string.missing_item)
    }

    @Suppress("unused")
    protected fun showMessage(@StringRes msgId: Int) {
        toastManagerData.postValue(msgId)
    }

    protected fun showMessage(msg: String) {
        toastManagerStringData.postValue(msg)
    }

    protected fun <RT> handleResultWithError(buildsResult: UseCaseResult<RT>, onSuccess: (RT) -> Unit) {
        buildsResult.error?.also { handleError(it) }
        buildsResult.result?.also { onSuccess(it) }
    }
}