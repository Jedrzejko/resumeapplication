package com.bovquier.resume.base.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedListAdapterCallback

abstract class BaseAdapter<TYPE, VH : BaseItemViewHolder<TYPE>, BACKED> : RecyclerView.Adapter<VH>() {
    abstract val items: BACKED
    abstract fun getItem(position: Int): TYPE
    abstract fun putItems(list: List<TYPE>?)

    abstract inner class BaseSortedCallback : SortedListAdapterCallback<TYPE>(this) {
        override fun areContentsTheSame(oldItem: TYPE?, newItem: TYPE?): Boolean = oldItem?.equals(newItem) ?: false
    }
}

abstract class BaseItemViewHolder<TYPE>(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: TYPE)
}