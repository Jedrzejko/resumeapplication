package com.bovquier.resume.base.view.viewmodels

import androidx.databinding.Bindable
import com.bovquier.resume.BR

abstract class BaseClickableItemViewModel<T> : BindingViewModel() {
    var item: T? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.itemSubTitle)
            notifyPropertyChanged(BR.itemTitle)
        }

    @get:Bindable
    val itemSubTitle
        get() = item?.let { getSubtitle(it).trim() }

    @get:Bindable
    val itemTitle
        get() = item?.let { getTitle(it).trim() }

    fun onItemClicked() {
        item?.also { performItemClick(it) }
    }

    protected abstract fun getTitle(item: T): String
    protected abstract fun getSubtitle(item: T): String
    protected abstract fun performItemClick(item: T)
}