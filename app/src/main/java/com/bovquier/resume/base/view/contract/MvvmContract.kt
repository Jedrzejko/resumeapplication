package com.bovquier.resume.base.view.contract

import androidx.lifecycle.LifecycleOwner
import com.bovquier.resume.base.view.viewmodels.BaseViewModel

interface MvvmContract<VIEW_MODEL : BaseViewModel> {
    val viewViewModel: VIEW_MODEL
    fun postCreate()
    fun observeViewModelData(viewLifecycleOwner: LifecycleOwner)
}
