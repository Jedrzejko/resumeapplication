package com.bovquier.resume.base.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.bovquier.resume.BR
import com.bovquier.resume.base.view.viewmodels.BindingViewModel

abstract class BindableAdapter<TYPE : Any, BINDING : ViewDataBinding, VH : AdapterItemViewHolder<BINDING, TYPE>, BAKED> : BaseAdapter<TYPE, VH, BAKED>() {
    abstract val layoutId: Int
    abstract fun getItemViewModel(): BindingViewModel
    abstract fun getViewHolder(dataBinding: BINDING): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH = LayoutInflater
        .from(parent.context)
        .let { DataBindingUtil.inflate<BINDING>(it, layoutId, parent, false) }
        .also { binding -> binding.setVariable(BR.viewModel, getItemViewModel()) }
        .let { getViewHolder(it) }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bindItem(getItem(position))
    }
}

abstract class ArrayBindableAdapter<TYPE : Any, BINDING : ViewDataBinding, VH : AdapterItemViewHolder<BINDING, TYPE>> : BindableAdapter<TYPE, BINDING, VH, ArrayList<TYPE>>() {
    override val items: ArrayList<TYPE> = arrayListOf()

    override fun getItem(position: Int): TYPE = items[position]

    override fun putItems(list: List<TYPE>?) {
        items.clear()
        list?.also { items.addAll(list) }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size
}

abstract class AdapterItemViewHolder<BINDING : ViewDataBinding, TYPE>(protected val binding: BINDING) : BaseItemViewHolder<TYPE>(binding.root) {
    fun bindItem(item: TYPE) {
        bind(item)
        binding.executePendingBindings()
    }
}