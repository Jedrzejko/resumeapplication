package com.bovquier.resume.base.api.repositories

import com.bovquier.resume.common.CommonExceptions.ApiCallException
import retrofit2.Response

abstract class BaseRepository {

    protected suspend fun <Result> performSimpleApiCall(call: suspend () -> Response<Result>): Result =
        call.invoke().let {
            if (!it.isSuccessful) throw ApiCallException.Builder(it.errorBody()).build()
            return it.body()!!
        }
}