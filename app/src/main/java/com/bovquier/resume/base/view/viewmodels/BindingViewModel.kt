package com.bovquier.resume.base.view.viewmodels

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BindingViewModel(private val callbacks: PropertyChangeRegistry = PropertyChangeRegistry()) : ViewModel(), Observable {

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }


    private val forceBindingData: MutableLiveData<Int> = MutableLiveData()

    val forceBinding: LiveData<Int> = forceBindingData

    fun forceBinding(fieldId: Int = -1) = forceBindingData.postValue(fieldId)
}
