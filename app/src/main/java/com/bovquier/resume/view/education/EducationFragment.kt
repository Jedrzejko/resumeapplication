package com.bovquier.resume.view.education

import android.os.Bundle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bovquier.resume.R
import com.bovquier.resume.base.view.fragments.BaseResumeMvvmFragment
import com.bovquier.resume.common.widget.VerticalCardMarginDecoration
import com.bovquier.resume.databinding.FragmentEducationBinding
import com.bovquier.resume.view.di.ViewScopesNames
import com.bovquier.resume.view.education.details.EducationDetailsFragment.Companion.KEY_EDUCATION_DETAILS_ID
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ViewModelOwner
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import kotlin.math.roundToInt

class EducationFragment : BaseResumeMvvmFragment<FragmentEducationBinding, EducationViewModel>() {

    private val itemClicker: ((Int) -> Unit) = { educationId ->
        val args = Bundle().apply {
            putInt(KEY_EDUCATION_DETAILS_ID, educationId)
        }
        findNavController().navigate(R.id.action_navigation_education_to_educationDetails, args)
    }


    override val layoutId: Int = R.layout.fragment_education
    override val viewScope: Scope = getKoin().getOrCreateScope(educationScopeIdentifier, named(ViewScopesNames.EDUCATION))
    override val viewViewModel by viewScope.viewModel<EducationViewModel>(owner = { ViewModelOwner.fromAny(this) })
    private val eduAdapter: EducationListingAdapter by viewScope.inject { parametersOf(itemClicker) }

    override fun observeViewModelData(viewLifecycleOwner: LifecycleOwner) {
        viewViewModel.adapterItems.observe(viewLifecycleOwner, Observer { eduAdapter.putItems(it) })
    }

    override fun postCreate() {
        setupRecycler()
        viewViewModel.loadEducation()
    }

    private fun setupRecycler() {
        with(binding.educationList) {
            this.setHasFixedSize(true)
            this.layoutManager = LinearLayoutManager(context)
            this.addItemDecoration(VerticalCardMarginDecoration(context.resources.getDimension(R.dimen.card_vertical_margin).roundToInt()))
            this.adapter = eduAdapter
        }
    }

    companion object {
        const val educationScopeIdentifier = "com.bovquier.resume.view.education"
    }
}