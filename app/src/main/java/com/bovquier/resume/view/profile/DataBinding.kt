package com.bovquier.resume.view.profile

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation


@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(imageUrl: String?) {
    imageUrl?.also {
        val progressDrawable: Drawable = CircularProgressDrawable(context)
            .apply { start() }
        Picasso.with(context)
            .apply { cancelRequest(this@setImageUrl) }
            .load(imageUrl)
            .placeholder(progressDrawable)
            .transform(CropCircleTransformation())
            .into(this)
    }
}