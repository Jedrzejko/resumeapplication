package com.bovquier.resume.view.experience

import android.text.format.DateFormat
import androidx.annotation.StringRes
import androidx.recyclerview.widget.SortedList
import com.bovquier.resume.R
import com.bovquier.resume.base.view.adapter.ListingAdapterViewHolder
import com.bovquier.resume.base.view.adapter.SortedAdapter
import com.bovquier.resume.base.view.viewmodels.BaseClickableItemViewModel
import com.bovquier.resume.base.view.viewmodels.BindingViewModel
import com.bovquier.resume.common.ResourceController
import com.bovquier.resume.data.db.IExperience
import com.bovquier.resume.databinding.ItemClickableBasicBinding
import java.util.*

class ExperienceListingAdapter(
    private val itemClicker: ((Int) -> Unit),
    private val resourceController: ResourceController
) : SortedAdapter<IExperience, ExperienceItemViewHolder>() {
    override val items: SortedList<IExperience> = SortedExperienceList()
    override fun getItemViewModel(): BindingViewModel = ExperienceItemViewModel(itemClicker, resourceController)
    override fun getViewHolder(dataBinding: ItemClickableBasicBinding): ExperienceItemViewHolder = ExperienceItemViewHolder(dataBinding)
    fun update(list: List<IExperience>?) {
        items.beginBatchedUpdates()
        list?.also {
            for (i in itemCount - 1 downTo 0) {
                if (list.none { it.experienceId == items[i].experienceId }) items.removeItemAt(i)
            }
            items.addAll(list)
        }
        items.endBatchedUpdates()
    }

    inner class SortedExperienceList : SortedList<IExperience>(IExperience::class.java, object : BaseSortedCallback() {
        override fun areItemsTheSame(item1: IExperience?, item2: IExperience?): Boolean = item1?.jobStart == item2?.jobStart
                && item1?.jobTitle == item2?.jobTitle
                && item1?.company?.companyName == item2?.company?.companyName

        override fun compare(o1: IExperience?, o2: IExperience?): Int = experienceComparator.compare(o1, o2)
    })

    companion object {
        private val experienceComparator = Comparator<IExperience> { o1, o2 -> (-1) * o1.jobStart.compareTo(o2.jobStart) }
    }
}

class ExperienceItemViewModel(private val itemClicker: ((Int) -> Unit), private val resourceController: ResourceController) : BaseClickableItemViewModel<IExperience>() {
    override fun getTitle(item: IExperience): String = "${item.jobTitle} : ${item.company.companyName}"
    override fun getSubtitle(item: IExperience): String = "${extractDate(item.jobStart, R.string.n_a)} - ${extractDate(item.jobEnd, R.string.date_present)}"

    private fun extractDate(date: Date?, @StringRes defaultValue: Int): CharSequence = date?.let { getYearMonth(it) } ?: resourceController.getString(defaultValue)

    private fun getYearMonth(date: Date): CharSequence = DateFormat
        .format(DateFormat.getBestDateTimePattern(Locale.getDefault(), "yyyy/MMM"), date)

    override fun performItemClick(item: IExperience) {
        itemClicker.invoke(item.experienceId)
    }
}

class ExperienceItemViewHolder(binding: ItemClickableBasicBinding) : ListingAdapterViewHolder<IExperience>(binding) {
    override fun bind(item: IExperience) {
        binding.viewModel?.item = item
    }
}