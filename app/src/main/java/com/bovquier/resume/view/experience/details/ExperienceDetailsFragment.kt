package com.bovquier.resume.view.experience.details


import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.bovquier.resume.R
import com.bovquier.resume.base.view.fragments.BaseResumeMvvmFragment
import com.bovquier.resume.common.argument
import com.bovquier.resume.common.widget.GridSpacingDecorator
import com.bovquier.resume.databinding.FragmentExperienceDetailsBinding
import com.bovquier.resume.view.di.ViewScopesNames
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ViewModelOwner
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import kotlin.math.roundToInt

class ExperienceDetailsFragment : BaseResumeMvvmFragment<FragmentExperienceDetailsBinding, ExperienceDetailsViewModel>() {
    override val layoutId: Int = R.layout.fragment_experience_details
    override val viewScope: Scope = getKoin().getOrCreateScope(scopeIdentifier, named(ViewScopesNames.EXPERIENCE))
    override val viewViewModel by viewScope.viewModel<ExperienceDetailsViewModel>(owner = { ViewModelOwner.fromAny(this) })
    private val framsAdapter: FrameworksAdapter by viewScope.inject()

    private val experienceId by argument(KEY_EXPERIENCE_DETAILS_ID, -1)

    override fun postCreate() {
        setupRecycler()
        viewViewModel.loadExperienceDetails(experienceId)
    }

    private fun setupRecycler() {
        with(binding.usedFrameworks) {
            this.setHasFixedSize(true)
            this.layoutManager = GridLayoutManager(context, 3)
            this.addItemDecoration(GridSpacingDecorator((context.resources.getDimension(R.dimen.card_vertical_margin).roundToInt())))
            this.adapter = framsAdapter
        }
    }

    override fun observeViewModelData(viewLifecycleOwner: LifecycleOwner) {
        viewViewModel.companyLinkClick.observe(viewLifecycleOwner, Observer { action -> action.value?.also { openCompanyPage(it) } })
        viewViewModel.jobFrameworsk.observe(viewLifecycleOwner, Observer { framsAdapter.putItems(it.toList()) })
    }

    private fun openCompanyPage(link: String) {
        CustomTabsIntent.Builder().build().launchUrl(requireContext(), Uri.parse(link))
    }

    companion object {
        const val KEY_EXPERIENCE_DETAILS_ID = "ExperienceDetailsFragment::KEY_EXPERIENCE_DETAILS_ID"
        const val scopeIdentifier = "com.bovquier.resume.view.experience.details"
    }
}
