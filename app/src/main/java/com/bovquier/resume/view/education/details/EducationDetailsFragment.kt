package com.bovquier.resume.view.education.details

import androidx.lifecycle.LifecycleOwner
import com.bovquier.resume.R
import com.bovquier.resume.base.view.fragments.BaseResumeMvvmFragment
import com.bovquier.resume.common.argument
import com.bovquier.resume.databinding.FragmentEducationDetailsBinding
import com.bovquier.resume.view.di.ViewScopesNames
import com.bovquier.resume.view.education.EducationFragment.Companion.educationScopeIdentifier
import org.koin.androidx.viewmodel.ViewModelOwner
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import org.koin.java.KoinJavaComponent.getKoin

class EducationDetailsFragment : BaseResumeMvvmFragment<FragmentEducationDetailsBinding, EducationDetailsViewModel>() {
    private val educationID: Int by argument(KEY_EDUCATION_DETAILS_ID, -1)
    override val viewScope: Scope = getKoin().getOrCreateScope(educationScopeIdentifier, named(ViewScopesNames.EDUCATION))
    override val layoutId: Int = R.layout.fragment_education_details
    override val viewViewModel by viewScope.viewModel<EducationDetailsViewModel>(owner = { ViewModelOwner.fromAny(this) }) { parametersOf(educationID) }

    override fun observeViewModelData(viewLifecycleOwner: LifecycleOwner) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun postCreate() {
        viewViewModel.loadEducationDetails()
    }

    companion object {
        const val KEY_EDUCATION_DETAILS_ID = "EducationDetailsFragment::KEY_EDUCATION_DETAILS_ID"
    }
}