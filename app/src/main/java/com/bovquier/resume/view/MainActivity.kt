package com.bovquier.resume.view

import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bovquier.resume.R
import com.bovquier.resume.api.di.clientModules
import com.bovquier.resume.api.di.connectorsModule
import com.bovquier.resume.api.di.databaseModule
import com.bovquier.resume.base.view.activties.BaseResumeMvvmActivity
import com.bovquier.resume.databinding.ActivityMainBinding
import com.bovquier.resume.domain.usecases.main.SecretsUseCase.SecretsState
import com.bovquier.resume.view.di.mainContainerModule
import com.bovquier.resume.view.di.viewScreensModule
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseResumeMvvmActivity<ActivityMainBinding, MainViewModel>() {
    override val layoutId: Int = R.layout.activity_main

    override val viewViewModel: MainViewModel by viewModel()

    override fun postCreate() {
        // no-op
    }

    override fun observeViewModelData(viewLifecycleOwner: LifecycleOwner) {
        viewViewModel.getSecretsState().observe(viewLifecycleOwner, Observer { manageState(it) })
    }

    private fun manageState(state: SecretsState?) = when (state) {
        SecretsState.SecretsRetrieved -> setupNavigation()
        SecretsState.SecretsError -> Toast.makeText(this, R.string.unauthorized, Toast.LENGTH_LONG).show()
        else -> Toast.makeText(this, R.string.unauthorized, Toast.LENGTH_LONG).show()
        // TODO: Add some retry on fetching secrets with alert dialog
    }

    private fun setupNavigation() {
        getKoin().loadModules(
            listOf(
                mainContainerModule,
                viewScreensModule,
                clientModules,
                connectorsModule,
                databaseModule
            )
        )
        val navController = findNavController(R.id.nav_host_fragment)
        navController.setGraph(R.navigation.mobile_navigation)
        val appBarConfiguration = AppBarConfiguration(
            setOf(R.id.navigation_profile, R.id.navigation_experience, R.id.navigation_education)
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)
        nav_view.isVisible = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> findNavController(R.id.nav_host_fragment).popBackStack()
        else -> super.onOptionsItemSelected(item)
    }
}
