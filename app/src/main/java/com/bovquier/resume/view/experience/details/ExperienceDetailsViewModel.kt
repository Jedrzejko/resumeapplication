package com.bovquier.resume.view.experience.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.common.LiveDataAction
import com.bovquier.resume.data.db.IExperience
import com.bovquier.resume.data.db.IFramework
import com.bovquier.resume.domain.usecases.experience.ExperienceDetailsUseCase

class ExperienceDetailsViewModel(private val useCase: ExperienceDetailsUseCase) : BaseViewModel() {
    private val experienceData = MutableLiveData<IExperience>()
    private val companyClickData = MutableLiveData<String>()
    val companyLinkClick = Transformations.map(companyClickData) { LiveDataAction(it) }

    val companyName: LiveData<String> = Transformations.map(experienceData) { it.company.companyName }
    val jobTitle: LiveData<String> = Transformations.map(experienceData) { it.jobTitle }
    val jobCity: LiveData<String> = Transformations.map(experienceData) { it.jobCity }
    val jobDescription: LiveData<String> = Transformations.map(experienceData) { it.jobBackground.description }
    val jobFrameworsk: LiveData<Set<IFramework>> = Transformations.map(experienceData) { it.jobBackground.usedFrameworks }

    fun loadExperienceDetails(experienceId: Int) {
        useCase.execute(experienceId) { result -> handleResultWithError(result) { experienceData.postValue(it) } }
    }

    fun onCompanyLinkClicked() {
        companyClickData.postValue(experienceData.value?.company?.companyWebsite)
    }
}
