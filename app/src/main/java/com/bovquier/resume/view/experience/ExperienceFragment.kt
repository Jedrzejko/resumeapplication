package com.bovquier.resume.view.experience

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bovquier.resume.R
import com.bovquier.resume.base.view.fragments.BaseResumeMvvmFragment
import com.bovquier.resume.common.widget.VerticalCardMarginDecoration
import com.bovquier.resume.databinding.FragmentExperienceBinding
import com.bovquier.resume.view.di.ViewScopesNames
import com.bovquier.resume.view.experience.details.ExperienceDetailsFragment.Companion.KEY_EXPERIENCE_DETAILS_ID
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ViewModelOwner
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import kotlin.math.roundToInt

class ExperienceFragment : BaseResumeMvvmFragment<FragmentExperienceBinding, ExperienceViewModel>() {

    private val itemClicker: ((Int) -> Unit) = { expId ->
        val args = Bundle().apply {
            putInt(KEY_EXPERIENCE_DETAILS_ID, expId)
        }
        findNavController().navigate(R.id.action_navigation_experience_to_experience_details_fragment, args)
    }

    override val viewScope: Scope = getKoin().getOrCreateScope(scopeIdentifier, named(ViewScopesNames.EXPERIENCE))
    override val viewViewModel by viewScope.viewModel<ExperienceViewModel>(owner = { ViewModelOwner.fromAny(this) })
    private val expAdapter: ExperienceListingAdapter by viewScope.inject { parametersOf(itemClicker) }

    override val layoutId: Int = R.layout.fragment_experience

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.exp_search, menu)
        val searchItem: MenuItem? = menu.findItem(R.id.action_search)
        searchItem?.run {
            val searchView = actionView as SearchView
            searchView.queryHint = getString(R.string.op_menu_search_hint)
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    viewViewModel.filterItems(query)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    if (newText.isNullOrBlank()) viewViewModel.filterItems(null)
                    if (newText?.length ?: 0 > 0) viewViewModel.filterItems(newText)
                    return true
                }
            })
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun postCreate() {
        setupRecycler()
        viewViewModel.loadExperience()
    }

    private fun setupRecycler() {
        with(binding.experienceList) {
            this.setHasFixedSize(true)
            this.layoutManager = LinearLayoutManager(context)
            this.addItemDecoration(VerticalCardMarginDecoration(context.resources.getDimension(R.dimen.card_vertical_margin).roundToInt()))
            this.adapter = expAdapter
        }
    }

    override fun observeViewModelData(viewLifecycleOwner: LifecycleOwner) {
        viewViewModel.experienceItems.observe(viewLifecycleOwner, Observer { expAdapter.putItems(it) })
        viewViewModel.filteredItems.observe(viewLifecycleOwner, Observer { expAdapter.update(it) })
    }

    companion object {
        private const val scopeIdentifier = "com.bovquier.resume.view.experience"
    }
}