package com.bovquier.resume.view.experience

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.data.db.IExperience
import com.bovquier.resume.domain.usecases.experience.ExperienceUseCase

// TODO: add savedState, loadExp in init{} - do not reload on back from details
class ExperienceViewModel(private val useCase: ExperienceUseCase) : BaseViewModel() {
    private val experience = mutableListOf<IExperience>()
    private val experienceData = MutableLiveData<List<IExperience>>()
    private val filterData = MutableLiveData<List<IExperience>>()
    val experienceItems: LiveData<List<IExperience>> = experienceData
    val filteredItems: LiveData<List<IExperience>> = filterData

    fun loadExperience() {
        useCase.execute("experience.json") { result ->
            handleResultWithError(result) { exp ->
                experience.replace(exp)
                experienceData.postValue(exp)
            }
        }
    }

    fun filterItems(query: String?) {
        query?.let {
            experience
                .filter { exp -> exp.jobBackground.usedFrameworks.any { item -> item.name.contains(query, true) } }
                .run { filterData.postValue(this) }
        } ?: filterData.postValue(experience)
    }
}

private fun <E> MutableList<E>.replace(list: List<E>) {
    clear()
    addAll(list)
}
