package com.bovquier.resume.view.profile

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.common.LiveDataAction
import com.bovquier.resume.data.pojo.ProfilePojo
import com.bovquier.resume.data.pojo.ProfilePojo.Language
import com.bovquier.resume.data.pojo.ProfilePojo.Profile
import com.bovquier.resume.data.pojo.ProfilePojo.SocialLink
import com.bovquier.resume.domain.usecases.profile.ProfileUseCase
import com.bovquier.resume.domain.usecases.profile.ProfileUseCaseParams

class ProfileViewModel(private val profileUseCase: ProfileUseCase) : BaseViewModel() {

    private val profileData = MutableLiveData<Profile>()
    val contactCombined: LiveData<String> = Transformations.map(profileData) { buildContact(it.contact) }
    val contactVisibility: LiveData<Int> = Transformations.map(contactCombined) { if (it.isNullOrBlank()) GONE else VISIBLE }

    val languagesData: LiveData<List<Language>> = Transformations.map(profileData) { it.languages }
    val languagesVisibility: LiveData<Int> = Transformations.map(languagesData) { if (it.isNullOrEmpty()) GONE else VISIBLE }

    val socials: LiveData<List<SocialLink>> = Transformations.map(profileData) { it.contact.social }
    val socialsVisibility: LiveData<Int> = Transformations.map(socials) { if (it.isNullOrEmpty()) GONE else VISIBLE }

    val profilePhoto: LiveData<String> = Transformations.map(profileData) { it.contact.photoUrl }

    val about = Transformations.map(profileData) { it.about }

    private val socialClickData = MutableLiveData<SocialLink>()
    val socialClick: LiveData<LiveDataAction<SocialLink>> = Transformations.map(socialClickData) { LiveDataAction(it) }

    private fun buildContact(contact: ProfilePojo.Contact?): String? = contact?.let {
        "${it.phoneNumber}\n${it.email}"
    }

    fun loadProfileData() {
        profileUseCase.execute(ProfileUseCaseParams(requestPath = "profile.json")) { result -> handleResultWithError(result) { profileData.postValue(it) } }
    }

    fun onSocialClicked(socialLink: SocialLink) {
        socialClickData.postValue(socialLink)
    }
}