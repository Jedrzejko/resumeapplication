package com.bovquier.resume.view.di

import com.bovquier.resume.api.repositories.EducationRepository
import com.bovquier.resume.api.repositories.ExperienceRepository
import com.bovquier.resume.api.repositories.ProfileRepository
import com.bovquier.resume.api.repositories.SecretsRepository
import com.bovquier.resume.domain.usecases.education.EducationDetailsUseCase
import com.bovquier.resume.domain.usecases.education.EducationUseCase
import com.bovquier.resume.domain.usecases.experience.ExperienceDetailsUseCase
import com.bovquier.resume.domain.usecases.experience.ExperienceUseCase
import com.bovquier.resume.domain.usecases.main.SecretsUseCase
import com.bovquier.resume.domain.usecases.profile.ProfileUseCase
import com.bovquier.resume.view.MainViewModel
import com.bovquier.resume.view.education.EducationListingAdapter
import com.bovquier.resume.view.education.EducationViewModel
import com.bovquier.resume.view.education.details.EducationDetailsViewModel
import com.bovquier.resume.view.experience.ExperienceListingAdapter
import com.bovquier.resume.view.experience.ExperienceViewModel
import com.bovquier.resume.view.experience.details.ExperienceDetailsViewModel
import com.bovquier.resume.view.experience.details.FrameworksAdapter
import com.bovquier.resume.view.profile.LanguageAdapter
import com.bovquier.resume.view.profile.ProfileViewModel
import com.bovquier.resume.view.profile.SocialAdapter
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val viewScreensModule = module {
    scope(named(ViewScopesNames.EDUCATION)) {
        scoped { EducationUseCase(get(), get()) }
        scoped { EducationRepository(get(), get()) }
        scoped { EducationDetailsUseCase(get(), get()) }
        scoped { (itemClicker: ((Int) -> Unit)) -> EducationListingAdapter(itemClicker) }
        viewModel { EducationViewModel(get()) }
        viewModel { (educationId: Int) -> EducationDetailsViewModel(educationId, get()) }
    }

    scope(named(ViewScopesNames.PROFILE)) {
        scoped { ProfileRepository(get()) }
        scoped { ProfileUseCase(get(), get()) }
        scoped { LanguageAdapter() }
        scoped { (viewModel: ProfileViewModel) -> SocialAdapter(viewModel, get()) }
        viewModel { ProfileViewModel(get()) }
    }

    scope(named(ViewScopesNames.EXPERIENCE)) {
        scoped { ExperienceUseCase(get(), get()) }
        scoped { ExperienceRepository(get(), get()) }
        scoped { ExperienceDetailsUseCase(get(), get()) }
        scoped { FrameworksAdapter() }
        scoped { (itemClicker: ((Int) -> Unit)) -> ExperienceListingAdapter(itemClicker, get()) }
        viewModel { ExperienceDetailsViewModel(get()) }
        viewModel { ExperienceViewModel(get()) }
    }
}

val mainContainerModule = module {
    viewModel { MainViewModel(get()) }
    factory { SecretsUseCase(get(), get()) }
    factory { SecretsRepository(Dispatchers.IO) }
}

object ViewScopesNames {
    const val EDUCATION = "view.education::EDUCATION"
    const val EXPERIENCE = "view.experience::EXPERIENCE"
    const val PROFILE = "view.profile::PROFILE"
}
