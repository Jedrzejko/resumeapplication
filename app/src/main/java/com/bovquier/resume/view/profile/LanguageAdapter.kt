package com.bovquier.resume.view.profile

import androidx.databinding.Bindable
import com.bovquier.resume.BR
import com.bovquier.resume.R
import com.bovquier.resume.base.view.adapter.AdapterItemViewHolder
import com.bovquier.resume.base.view.adapter.ArrayBindableAdapter
import com.bovquier.resume.base.view.viewmodels.BindingViewModel
import com.bovquier.resume.data.pojo.ProfilePojo.Language
import com.bovquier.resume.databinding.ItemLanguageBinding

class LanguageAdapter : ArrayBindableAdapter<Language, ItemLanguageBinding, LanguageViewHolder>() {
    override val layoutId: Int = R.layout.item_language
    override fun getItemViewModel(): BindingViewModel = LanguageItemViewModel()
    override fun getViewHolder(dataBinding: ItemLanguageBinding): LanguageViewHolder = LanguageViewHolder(dataBinding)
}

class LanguageViewHolder(binding: ItemLanguageBinding) : AdapterItemViewHolder<ItemLanguageBinding, Language>(binding) {
    override fun bind(item: Language) {
        binding.viewModel?.item = item
    }

}

class LanguageItemViewModel : BindingViewModel() {
    var item: Language? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.langRating)
            notifyPropertyChanged(BR.langName)
        }

    @get:Bindable
    val langRating
        get() = item?.languageLevel

    @get:Bindable
    val langName
        get() = item?.language
}