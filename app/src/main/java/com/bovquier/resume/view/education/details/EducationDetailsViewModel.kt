package com.bovquier.resume.view.education.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.domain.usecases.education.EducationDetailsUseCase

class EducationDetailsViewModel(
    private val educationId: Int,
    private val eduDetailsUseCase: EducationDetailsUseCase
) : BaseViewModel() {

    private val educationData = MutableLiveData<IEducation>()

    val schoolName: LiveData<String> = Transformations.map(educationData) { it.school }
    val schoolFinishResult: LiveData<String> = Transformations.map(educationData) { "${it.finishYear} - ${it.title}" }
    val schoolFos: LiveData<String> = Transformations.map(educationData) { it.fos }
    val subjectTitle: LiveData<String> = Transformations.map(educationData) { it.project.projectName }
    val subjectDescription: LiveData<String> = Transformations.map(educationData) { it.project.projectDescription }

    fun loadEducationDetails() {
        eduDetailsUseCase.execute(educationId) { result -> handleResultWithError(result) { educationData.postValue(it) } }
    }

}
