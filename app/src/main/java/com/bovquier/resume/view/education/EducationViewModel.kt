package com.bovquier.resume.view.education

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.domain.usecases.ActionType.FOR_ERROR
import com.bovquier.resume.domain.usecases.education.EducationUseCase
import com.bovquier.resume.domain.usecases.education.EducationUseCaseParams

// TODO: add savedState, loadExp in init{} - do not reload on back from details
class EducationViewModel(private val useCase: EducationUseCase) : BaseViewModel() {

    private val adapterData: MutableLiveData<List<IEducation>> = MutableLiveData()
    val adapterItems: LiveData<List<IEducation>> = adapterData

    fun loadEducation() {
        useCase.execute(EducationUseCaseParams(loadPath = EDU_PATH)) { result ->
            handleResultWithError(result) { adapterData.postValue(it) }
        }
    }

    fun loadFakeData() {
        useCase.execute(EducationUseCaseParams(actionType = FOR_ERROR, loadPath = FAKE_PATH)) { result ->
            handleResultWithError(result) { adapterData.postValue(it) }
        }
    }

    companion object {
        const val EDU_PATH = "education.json"
        const val FAKE_PATH = "fake.path"
    }
}