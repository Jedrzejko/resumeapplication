package com.bovquier.resume.view.experience.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bovquier.resume.R
import com.bovquier.resume.base.view.adapter.BaseAdapter
import com.bovquier.resume.base.view.adapter.BaseItemViewHolder
import com.bovquier.resume.data.db.IFramework
import com.bovquier.resume.view.experience.details.FrameworksAdapter.FrameworkViewHolder
import kotlinx.android.synthetic.main.item_framework.view.*

class FrameworksAdapter : BaseAdapter<IFramework, FrameworkViewHolder, ArrayList<IFramework>>() {
    override val items: ArrayList<IFramework> = arrayListOf()

    override fun getItem(position: Int): IFramework = items[position]
    override fun getItemCount(): Int = items.size

    override fun putItems(list: List<IFramework>?) {
        items.clear()
        list?.also { items.addAll(list) }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FrameworkViewHolder = LayoutInflater.from(parent.context)
        .inflate(R.layout.item_framework, parent, false)
        .let { FrameworkViewHolder(it) }

    override fun onBindViewHolder(holder: FrameworkViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class FrameworkViewHolder(view: View) : BaseItemViewHolder<IFramework>(view) {
        override fun bind(item: IFramework) {
            itemView.frameworkName.text = item.name
        }
    }
}
