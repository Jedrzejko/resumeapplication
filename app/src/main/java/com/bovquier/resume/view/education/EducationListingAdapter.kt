package com.bovquier.resume.view.education

import androidx.recyclerview.widget.SortedList
import com.bovquier.resume.base.view.adapter.ListingAdapterViewHolder
import com.bovquier.resume.base.view.adapter.SortedAdapter
import com.bovquier.resume.base.view.viewmodels.BaseClickableItemViewModel
import com.bovquier.resume.base.view.viewmodels.BindingViewModel
import com.bovquier.resume.data.db.IEducation
import com.bovquier.resume.databinding.ItemClickableBasicBinding

class EducationListingAdapter(private val itemClicker: ((Int) -> Unit)) : SortedAdapter<IEducation, EducationItemViewHolder>() {
    override val items: SortedList<IEducation> = EducationList()
    override fun getItemViewModel(): BindingViewModel = EducationItemViewModel(itemClicker)
    override fun getViewHolder(dataBinding: ItemClickableBasicBinding): EducationItemViewHolder = EducationItemViewHolder(dataBinding)

    inner class EducationList : SortedList<IEducation>(IEducation::class.java, object : BaseSortedCallback() {
        override fun areItemsTheSame(item1: IEducation?, item2: IEducation?): Boolean = item1?.finishYear == item2?.finishYear && item1?.fos == item2?.fos

        override fun compare(o1: IEducation?, o2: IEducation?): Int = educationComparator.compare(o1, o2)
    })

    companion object {
        private val educationComparator = Comparator<IEducation> { o1, o2 -> o1.finishYear.compareTo(o2.finishYear) }
    }
}

class EducationItemViewModel(private val itemClicker: ((Int) -> Unit)) : BaseClickableItemViewModel<IEducation>() {
    override fun getTitle(item: IEducation): String = item.school
    override fun getSubtitle(item: IEducation): String = "${item.finishYear} - ${item.title}"
    override fun performItemClick(item: IEducation) = itemClicker.invoke(item.educationId)
}

class EducationItemViewHolder(binding: ItemClickableBasicBinding) : ListingAdapterViewHolder<IEducation>(binding) {
    override fun bind(item: IEducation) {
        binding.viewModel?.item = item
    }
}
