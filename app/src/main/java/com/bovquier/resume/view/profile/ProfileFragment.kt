package com.bovquier.resume.view.profile

import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bovquier.resume.R
import com.bovquier.resume.base.view.fragments.BaseResumeMvvmFragment
import com.bovquier.resume.common.widget.HorizontalCardMarginDecoration
import com.bovquier.resume.common.widget.VerticalCardMarginDecoration
import com.bovquier.resume.databinding.FragmentProfileBinding
import com.bovquier.resume.view.di.ViewScopesNames
import org.koin.android.ext.android.getKoin
import org.koin.androidx.viewmodel.ViewModelOwner
import org.koin.androidx.viewmodel.scope.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.core.scope.Scope
import kotlin.math.roundToInt


class ProfileFragment : BaseResumeMvvmFragment<FragmentProfileBinding, ProfileViewModel>() {
    override val viewScope: Scope = getKoin().getOrCreateScope(profileScopeIdentifier, named(ViewScopesNames.PROFILE))
    override val layoutId: Int = R.layout.fragment_profile
    override val viewViewModel by viewScope.viewModel<ProfileViewModel>(owner = { ViewModelOwner.fromAny(this) })
    private val langAdapter by viewScope.inject<LanguageAdapter>()
    private val socialAdapter by viewScope.inject<SocialAdapter> { parametersOf(viewViewModel) }

    private fun openSocialTab(link: String) {
        CustomTabsIntent.Builder().build().launchUrl(requireContext(), Uri.parse(link))
    }

    override fun observeViewModelData(viewLifecycleOwner: LifecycleOwner) {
        viewViewModel.languagesData.observe(viewLifecycleOwner, Observer { langAdapter.putItems(it) })
        viewViewModel.socials.observe(viewLifecycleOwner, Observer { socialAdapter.putItems(it) })
        viewViewModel.socialClick.observe(viewLifecycleOwner, Observer { action -> action.value?.also { openSocialTab(it.link) } })
    }

    override fun postCreate() {
        setupRecyclers()
        viewViewModel.loadProfileData()
    }

    private fun setupRecyclers() {
        with(binding.languages) {
            this.setHasFixedSize(true)
            this.layoutManager = LinearLayoutManager(context)
            this.addItemDecoration(VerticalCardMarginDecoration(context.resources.getDimension(R.dimen.card_vertical_margin).roundToInt()))
            this.adapter = langAdapter
        }
        with(binding.socialLinks) {
            this.setHasFixedSize(true)
            this.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            this.addItemDecoration(HorizontalCardMarginDecoration(context.resources.getDimension(R.dimen.card_vertical_margin).roundToInt()))
            this.adapter = socialAdapter
        }
    }

    companion object {
        const val profileScopeIdentifier = "com.bovquier.resume.view.profile"
    }
}