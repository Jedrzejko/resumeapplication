package com.bovquier.resume.view.profile

import androidx.databinding.Bindable
import com.bovquier.resume.BR
import com.bovquier.resume.R
import com.bovquier.resume.base.view.adapter.AdapterItemViewHolder
import com.bovquier.resume.base.view.adapter.ArrayBindableAdapter
import com.bovquier.resume.base.view.viewmodels.BindingViewModel
import com.bovquier.resume.common.ResourceController
import com.bovquier.resume.data.pojo.ProfilePojo.SocialLink
import com.bovquier.resume.databinding.ItemSocialLinkBinding

class SocialAdapter(private val viewModel: ProfileViewModel, private val resourceController: ResourceController) :
    ArrayBindableAdapter<SocialLink, ItemSocialLinkBinding, SocialITemViewHolder>() {
    override val layoutId: Int = R.layout.item_social_link
    override fun getItemViewModel(): BindingViewModel = SocialLinkItemViewModel(viewModel, resourceController)
    override fun getViewHolder(dataBinding: ItemSocialLinkBinding): SocialITemViewHolder = SocialITemViewHolder(dataBinding)
}

class SocialITemViewHolder(binding: ItemSocialLinkBinding) : AdapterItemViewHolder<ItemSocialLinkBinding, SocialLink>(binding) {
    override fun bind(item: SocialLink) {
        binding.viewModel?.item = item
    }
}

class SocialLinkItemViewModel(private val viewModel: ProfileViewModel, private val resourceController: ResourceController) : BindingViewModel() {
    var item: SocialLink? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.socialIcon)
        }

    @get:Bindable
    val socialIcon
        get() = resourceController.getSocialDrawable(item?.socialType?.logoId)

    fun onSocialClicked() {
        item?.also { viewModel.onSocialClicked(it) }
    }
}