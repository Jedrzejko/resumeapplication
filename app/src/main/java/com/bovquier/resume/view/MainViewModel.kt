package com.bovquier.resume.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bovquier.resume.base.view.viewmodels.BaseViewModel
import com.bovquier.resume.domain.usecases.main.SecretsUseCase

class MainViewModel(secretsUseCase: SecretsUseCase) : BaseViewModel() {

    private val secretsStateData = MutableLiveData<SecretsUseCase.SecretsState>()
    fun getSecretsState(): LiveData<SecretsUseCase.SecretsState> = secretsStateData

    init {
        secretsUseCase.fetchSecrets { secretsStateData.postValue(it) }
    }
}