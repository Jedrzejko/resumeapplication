# Resume

## Requirements
* Android Studio `3.5+`
* JDK `1.8+`
* \* Spek `2.+` plugins, to run test manually from IDE

## Release info
* Local release build available only when provided own signing config. In-app signing config is provided by CI.

## Running tests

* To run code checks from console, run
``` 
Unix:
./gradlew fullCheck
```
```
Windows:
gradlew fullCheck
```
this will perform `detekt` static analysis basic `lint`, run `jUnit tests` on Debug

## Questions
For questions please contact:
* jedrzejko.michal@gmail.com
* or post an [Issue](https://gitlab.com/Jedrzejko/resumeapplication/issues/new)
